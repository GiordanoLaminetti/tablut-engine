DIR=`dirname "$0"`

case "$0" in 
  /*) PROJECT_DIR=$DIR ;;
  *) PROJECT_DIR=`pwd`/$DIR;;
esac


cd $PROJECT_DIR

cargo run --release --bin tablut_competition_player -- $*
