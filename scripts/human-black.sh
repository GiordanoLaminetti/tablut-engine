#!/bin/bash

if [ -d /tmp/tablut-server ]; then
  cd /tmp/tablut-server/Tablut
  ant humanblack
else
  echo "Error: folder \"tmp/server\" not found"
  exit 1
fi
