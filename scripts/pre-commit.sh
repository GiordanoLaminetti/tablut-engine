#!/usr/bin/bash

echo "Running pre-commit hooks"

echo "Building..."

cargo build
if [ $? -ne 0 ]; then
  echo "Build failed!"
  exit 1
fi

echo "Running tests..."
cargo test
if [ $? -ne 0 ]; then
  exit 1
fi 
