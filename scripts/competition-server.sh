#!/bin/bash

if ! [ -d /tmp/tablut-server ]; then
        git clone https://GiordanoLaminetti@bitbucket.org/GiordanoLaminetti/tablutcompetition.git  /tmp/tablut-server
        cd /tmp/tablut-server/Tablut
        ant clean
        ant compile
fi

cd /tmp/tablut-server/Tablut
#ant clean
#ant compile
ant server
