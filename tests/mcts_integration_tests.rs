extern crate tablut_engine;

use rand;
use rand::Rng;
use std::process::{Command, Stdio};
use std::rc::Rc;
use std::thread;
use std::time::Duration;
use tablut_engine::environment::tablut_competition_env::TablutCompetionEnv as TEnv;
use tablut_engine::environment::GameEnv;
use tablut_engine::games::tablut;
use tablut_engine::games::tablut::game_board::*;
use tablut_engine::games::tablut::players::greedy_player::*;
use tablut_engine::games::tablut::{Board, Move};
use tablut_engine::player::mcts_player::{MonteCarloPlayer, SimulationGame, SimulationPlayer};
use tablut_engine::player::random_player::RandomPlayer;
use tablut_engine::player::{Game, GameResult, Player, Players};

fn start_server() {
    Command::new("scripts/competition-server.sh")
        .spawn()
        .unwrap();
}
fn start_random_white() {
    Command::new("scripts/random-white.sh").spawn().unwrap();
}

fn start_random_black() {
    Command::new("scripts/random-black.sh").spawn().unwrap();
}

// tests

#[test]
#[ignore]
fn test_mcts_white() {
    println!("AO");
    //start_server();
    println!("Server started!");

    thread::sleep(Duration::from_secs(10));

    //start_random_black();
    println!("Random black started!");

    //first connection
    let port = 5800;
    let name = "\"HillClimbers\"\n";
    let mut game_env = TEnv::from(format!("127.0.0.1:{}", port)).unwrap();

    // init mcts
    let game_tablut = BoardWhiteGame::new();
    let mut player = MonteCarloPlayer::new(
        false,
        BoardWhiteGame::new(),
        Board::init(),
        GreedyWhitePlayer::new(),
        GreedyBlackPlayer::new(),
        10,
        10,
    );

    println!("Inizio, invio Nome : {:?}", name);
    //send squad name
    match game_env.send_name(&name.to_string()) {
        Ok(_) => println!("Client: reply ok"),
        Err(error) => println!("Client: reply error: {}", error),
    }

    // receive state
    let mut response =
        GameEnv::<tablut::Move, tablut::TablutStateMessage>::response(&mut game_env).unwrap();
    while response.turn != "WHITEWIN" && response.turn != "BLACKWIN" && response.turn != "DRAW" {
        if response.turn == "WHITE" {
            // white turn
            let state = response.convert_state();
            //println!("State {:?}", state.table);

            let action = player.next_move(state).unwrap();
            println!("Moves = {:?}", action);
            //send moves
            match GameEnv::<tablut::Move, tablut::TablutStateMessage>::reply(&mut game_env, action)
            {
                Ok(_) => println!("Client: reply ok"),
                Err(error) => println!("Client: reply error: {}", error),
            }
        }
        // new state
        response =
            GameEnv::<tablut::Move, tablut::TablutStateMessage>::response(&mut game_env).unwrap();
    }
    println!("risultato =>  {:?}", response.turn);
    assert!(response.turn == "WHITEWIN" || response.turn == "BLACKWIN" || response.turn == "DRAW")
}
