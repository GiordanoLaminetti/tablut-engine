extern crate tablut_engine;

use std::process::{Command, Stdio};
use std::thread;
use std::time::Duration;
use tablut_engine::environment::tablut_competition_env::TablutCompetionEnv as TEnv;
use tablut_engine::environment::GameEnv;
use tablut_engine::games::tablut;
use tablut_engine::games::tablut::{Board, Move};
use tablut_engine::player::random_player::RandomPlayer;
use tablut_engine::player::Player;

fn start_server() {
    Command::new("scripts/competition-server.sh")
        .spawn()
        .unwrap();
}
fn start_random_white() {
    Command::new("scripts/random-white.sh").spawn().unwrap();
}

fn start_random_black() {
    Command::new("scripts/random-black.sh").spawn().unwrap();
}

#[test]
#[ignore]
fn test_random_white() {
    println!("AO");
    start_server();
    println!("Server started!");

    thread::sleep(Duration::from_secs(1));

    start_random_black();
    println!("Random black started!");

    thread::sleep(Duration::from_secs(1));

    //first connection
    let port = 5800;
    let name = "\"HillClimbers\"\n";
    let mut game_env = TEnv::from(format!("127.0.0.1:{}", port)).unwrap();

    //init generator
    let generator = |b: tablut::Board| {
        let mut a = Vec::new();
        a.extend(b.white_moves());
        a
    };
    let mut player = RandomPlayer::new(generator);

    println!("Inizio, invio Nome : {:?}", name);
    //send squad name
    match game_env.send_name(&name.to_string()) {
        Ok(_) => println!("Client: reply ok"),
        Err(error) => println!("Client: reply error: {}", error),
    }

    // receive state
    let mut response =
        GameEnv::<tablut::Move, tablut::TablutStateMessage>::response(&mut game_env).unwrap();
    while response.turn != "WHITEWIN" && response.turn != "BLACKWIN" && response.turn != "DRAW" {
        if response.turn == "WHITE" {
            // white turn
            let state = response.convert_state();
            //println!("State {:?}", state.table);

            let action = player.next_move(state).unwrap();
            println!("Moves = {:?}", action);
            //send moves
            match GameEnv::<tablut::Move, tablut::TablutStateMessage>::reply(&mut game_env, action)
            {
                Ok(_) => println!("Client: reply ok"),
                Err(error) => println!("Client: reply error: {}", error),
            }
        }
        // new state
        response =
            GameEnv::<tablut::Move, tablut::TablutStateMessage>::response(&mut game_env).unwrap();
    }
    println!("risultato =>  {:?}", response.turn);
    assert!(response.turn == "WHITEWIN" || response.turn == "BLACKWIN" || response.turn == "DRAW")
}

#[test]
#[ignore]
fn test_random_black() {
    println!("AO");
    start_server();
    println!("Server started!");

    thread::sleep(Duration::from_secs(1));

    start_random_white();
    println!("Random black started!");

    thread::sleep(Duration::from_secs(1));

    //first connection
    let port = 5801;
    let name = "\"HillClimbers\"\n";
    let mut game_env = TEnv::from(format!("127.0.0.1:{}", port)).unwrap();

    //init generator
    let generator = |b: tablut::Board| {
        let mut a = Vec::new();
        a.extend(b.black_moves());
        a
    };
    let mut player = RandomPlayer::new(generator);

    println!("Inizio, invio Nome : {:?}", name);
    //send squad name
    match game_env.send_name(&name.to_string()) {
        Ok(_) => println!("Client: reply ok"),
        Err(error) => println!("Client: reply error: {}", error),
    }

    // receive state
    let mut response =
        GameEnv::<tablut::Move, tablut::TablutStateMessage>::response(&mut game_env).unwrap();
    while response.turn != "WHITEWIN" && response.turn != "BLACKWIN" && response.turn != "DRAW" {
        if response.turn == "BLACK" {
            // black turn
            let state = response.convert_state();
            //println!("State {:?}", state.table);

            let action = player.next_move(state).unwrap();
            println!("Moves = {:?}", action);
            //send moves
            match GameEnv::<tablut::Move, tablut::TablutStateMessage>::reply(&mut game_env, action)
            {
                Ok(_) => println!("Client: reply ok"),
                Err(error) => println!("Client: reply error: {}", error),
            }
        }
        // new state
        response =
            GameEnv::<tablut::Move, tablut::TablutStateMessage>::response(&mut game_env).unwrap();
    }
    println!("risultato =>  {:?}", response.turn);
    assert!(response.turn == "WHITEWIN" || response.turn == "BLACKWIN" || response.turn == "DRAW")
}

#[test]
#[ignore]
fn test_random_match() {
    //WHITE
    thread::spawn(move || {
        //first connection
        let port = 5800;
        let name = "\"HillClimbersW\"\n";
        let mut game_env = TEnv::from(format!("127.0.0.1:{}", port)).unwrap();

        //init generator
        let generator = |b: tablut::Board| {
            let mut a = Vec::new();
            a.extend(b.white_moves());
            a
        };
        let mut player = RandomPlayer::new(generator);

        println!("Inizio W, invio Nome : {:?}", name);
        //send squad name
        match game_env.send_name(&name.to_string()) {
            Ok(_) => println!("Client W: reply ok"),
            Err(error) => println!("Client W: reply error: {}", error),
        }

        // receive state
        let mut response =
            GameEnv::<tablut::Move, tablut::TablutStateMessage>::response(&mut game_env).unwrap();
        while response.turn != "WHITEWIN" && response.turn != "BLACKWIN" && response.turn != "DRAW"
        {
            if response.turn == "WHITE" {
                // white turn
                let state = response.convert_state();
                //println!("State W {:?}", state.table);

                let action = player.next_move(state).unwrap();
                println!("Moves W = {:?}", action);
                //send moves
                match GameEnv::<tablut::Move, tablut::TablutStateMessage>::reply(
                    &mut game_env,
                    action,
                ) {
                    Ok(_) => println!("Client W: reply ok"),
                    Err(error) => println!("Client W: reply error: {}", error),
                }
            }
            // new state
            response = GameEnv::<tablut::Move, tablut::TablutStateMessage>::response(&mut game_env)
                .unwrap();
        }
    });

    //BLACK
    //first connection
    let port = 5801;
    let name = "\"HillClimbersB\"\n";
    let mut game_env = TEnv::from(format!("127.0.0.1:{}", port)).unwrap();

    //init generator
    let generator = |b: tablut::Board| {
        let mut a = Vec::new();
        a.extend(b.black_moves());
        a
    };
    let mut player = RandomPlayer::new(generator);

    println!("Inizio B, invio Nome : {:?}", name);
    //send squad name
    match game_env.send_name(&name.to_string()) {
        Ok(_) => println!("Client B: reply ok"),
        Err(error) => println!("Client B: reply error: {}", error),
    }

    // receive state
    let mut response =
        GameEnv::<tablut::Move, tablut::TablutStateMessage>::response(&mut game_env).unwrap();
    while response.turn != "WHITEWIN" && response.turn != "BLACKWIN" && response.turn != "DRAW" {
        if response.turn == "BLACK" {
            // black turn
            let state = response.convert_state();
            //println!("State B{:?}", state.table);

            let action = player.next_move(state).unwrap();
            println!("Moves B= {:?}", action);
            //send moves
            match GameEnv::<tablut::Move, tablut::TablutStateMessage>::reply(&mut game_env, action)
            {
                Ok(_) => println!("Client B: reply ok"),
                Err(error) => println!("Client B: reply error: {}", error),
            }
        }
        // new state
        response =
            GameEnv::<tablut::Move, tablut::TablutStateMessage>::response(&mut game_env).unwrap();
    }
    println!("risultato =>  {:?}", response.turn);
    assert!(response.turn == "WHITEWIN" || response.turn == "BLACKWIN" || response.turn == "DRAW")
}
