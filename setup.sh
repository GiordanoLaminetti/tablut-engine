#install dependencies
sudo apt-get install curl libssl-dev pkg-config gcc

#install rust
curl https://sh.rustup.rs -sSf > rustup-init 
chmod u+x rustup-init
./rustup-init -y
export PATH="$HOME/.cargo/bin:$PATH"
