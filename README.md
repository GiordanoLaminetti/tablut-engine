Tablut Player
=============
TODO

## Setup

You can install the required dependencies and build the player with the setup.sh script, alternatively follow the instructions for the manual setup which breaks down the installation steps in the script.

### Manual setup
Install the required packages:
```
$ sudo apt-get install curl libssl-dev pkg-config gcc 
```
Manually install the rust toolchain by downloading the rustup-init script, making it executable and running it:
```
$ curl https://sh.rustup.rs -sSf > rustup-init 
$ chmod u+x rustup-init
$ ./rustup-init -y
$ source $HOME/.cargo/env
$ scripts/build.sh
```
Check rust installation with the following command:
```
$ rustc -V
```
## Usage

Clone this bucket, checkout the version you need and cd into the dir.

WARNING: the competition player is tagged _competition_:

```
$ git checkout competition
```


WARNING: Since the repository contains big files required for neural networks we suggest you git clone the repo using ssh (this might still take a while):
```
$ git clone ssh://bitbucket.org/GiordanoLaminetti/tablut-engine.git
$ cd tablut-engine
```
In order to use ssh you need to set a public key into your bitbucket account settings.

Once you have cloned the repo and the setup is finished, you can run the player with the run.sh script passing "white" or "black" as first argument and the time limit with the "-t" option (by default the palyer will be playing with 50 seconds).

### Examples
Display help command showing the options:
```
$ ./run.sh -h
```
Output:
```
Usage: target/release/tablut_competition_player <white|black> [options]

Options:
    -c, --conf <CONF_FILE>
                        set configuration file
    -l, --log <LOG_FILE>
                        set log
    -s, --server <IP>   server ip
    -p, --port <PORT>   server port to bind to
    -t, --timeout <TIMEOUT>
                        set the player's timeout
        --white_cnn_path <PATH>
                        choose white cnn
        --black_cnn_path <PATH>
                        choose black cnn
    -h, --help          print this help
    -r, --report [[REPORT_FILE]]
                        print mcts_player reports
```

Run a white player

```
$ ./run.sh white
```
