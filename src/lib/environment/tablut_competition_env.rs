use crate::environment as env;
use crate::environment::GameEnv;
use serde::de::DeserializeOwned;
use serde::Serialize;
use serde_json;
use std::convert::TryFrom;
use std::io;
use std::io::prelude::{Read, Write};
use std::net;
use std::net::{TcpStream, ToSocketAddrs};

// Conversions for env::Error

impl From<serde_json::Error> for env::Error {
    fn from(_: serde_json::Error) -> Self {
        env::Error::Error(String::from("JSON conversion error"))
    }
}

impl From<env::Error> for io::Error {
    fn from(error: env::Error) -> Self {
        match error {
            env::Error::IoError(err) => err,
            env::Error::Error(descr) => io::Error::new(io::ErrorKind::Other, descr),
        }
    }
}

impl From<std::str::Utf8Error> for env::Error {
    fn from(_: std::str::Utf8Error) -> Self {
        env::Error::Error(String::from("UTF8 conversion error"))
    }
}

impl From<std::num::TryFromIntError> for env::Error {
    fn from(error: std::num::TryFromIntError) -> Self {
        env::Error::Error(String::from(format!("Integer conversion error: {}", error)))
    }
}

// TCP Environment with conversion in JSON //

pub struct TablutCompetionEnv {
    handle: net::TcpStream,
}

impl TablutCompetionEnv {
    pub fn from<A: ToSocketAddrs>(addr: A) -> io::Result<TablutCompetionEnv> {
        let handle = TcpStream::connect(addr)?;
        Ok(TablutCompetionEnv { handle })
    }
}

impl<A, B> GameEnv<A, B> for TablutCompetionEnv
where
    A: Serialize,
    B: DeserializeOwned,
{
    fn reply(&mut self, a: A) -> Result<(), env::Error> {
        let json = serde_json::to_string(&a)?;
        let length = u32::try_from(json.len())?;
        self.handle.write(&length.to_be_bytes())?;
        self.handle.write(json.as_bytes())?;
        Ok(())
    }

    fn response(&mut self) -> Result<B, env::Error> {
        // read length of json utf8 string
        let mut length = [0; 4];
        self.handle.read_exact(&mut length)?;
        let length = u32::from_be_bytes(length);

        // read json string and convert
        let mut read = vec![0; length as usize];
        self.handle.read_exact(&mut read)?;
        let to_parse = std::str::from_utf8(&read)?;
        let res: B = serde_json::from_str(to_parse)?;

        Ok(res)
    }
}
impl TablutCompetionEnv {
    pub fn send_name(&mut self, s: &str) -> Result<(), env::Error> {
        let length = u32::try_from(s.len())?;
        self.handle.write(&length.to_be_bytes())?;
        self.handle.write(s.as_bytes())?;
        Ok(())
    }
}

/* TESTS */
#[cfg(test)]
mod tests {

    use super::*;
    use net::TcpListener;
    use serde::Deserialize;
    use std::convert::TryFrom;
    use std::sync::mpsc::channel;
    use std::thread;
    use std::time::Duration;

    #[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
    struct SerdeMock {
        string: String,
    }

    impl SerdeMock {
        fn new(string: &str) -> SerdeMock {
            SerdeMock {
                string: String::from(string),
            }
        }
    }

    #[test]
    #[ignore]
    fn test_open_connection() {
        let port = 9000;
        thread::spawn(move || {
            let listener = TcpListener::bind(format!("0.0.0.0:{}", &port)).unwrap();
            println!("Server: listening on port {}", port);

            match listener.accept() {
                Ok((_stream, peer_addr)) => {
                    println!("Server: new connection: {}", peer_addr);
                    assert!(true);
                }
                Err(e) => {
                    panic!("Server: error: {}", e);
                }
            }
        });

        // prevents client connection before server is bound
        thread::sleep(Duration::from_millis(1));

        match TablutCompetionEnv::from(format!("0.0.0.0:{}", &port)) {
            Ok(_) => {
                println!("Client: connected");
                assert!(true);
            }
            Err(_error) => {
                println!("Client: error: {}", _error);
                assert!(false);
            }
        }
    }

    #[test]
    fn test_reply() {
        let port = 9001;
        let serde_mock = SerdeMock::new("Hello");
        //let serde_mock_clone = serde_mock.clone();

        let (sender, receiver) = channel();

        thread::spawn(move || {
            let server_mock = TcpListener::bind(format!("127.0.0.1:{}", port)).unwrap();
            match server_mock.accept() {
                Ok((mut stream, _)) => {
                    //println!("Server: new client: {:?}", addr);

                    let mut length = [0 as u8; 4];
                    stream.read(&mut length).unwrap();
                    //println!("Server: length {:?}", length);
                    let length = u32::from_be_bytes(length);
                    //println!("Server: length {:?}", length);

                    let mut read = vec![0 as u8; length as usize];
                    stream.read_exact(&mut read).unwrap();
                    //println!("Server: read {:?}", read);

                    let to_parse = std::str::from_utf8(&read).unwrap();
                    //println!("Server: parsed {:?}", to_parse);

                    let res: SerdeMock = serde_json::from_str(to_parse).unwrap();
                    //println!("Server: serde_mock and clone equals? {}", res == serde_mock_clone);
                    sender.send(res).unwrap();
                }
                Err(e) => println!("Server: couldn't get client: {:?}", e),
            }
        });
        thread::sleep(Duration::from_millis(1));

        let serialized_mock = serde_json::to_string(&serde_mock).unwrap();
        println!(
            "Client: serialized struct: {}, length: {}",
            serialized_mock,
            serialized_mock.len()
        );

        let mut game_env = TablutCompetionEnv::from(format!("127.0.0.1:{}", port));
        match &mut game_env {
            Ok(_) => {
                println!("Client: socket ok");
            }
            Err(error) => println!("Client: socket error: {}", error),
        }
        let another_serde_mock_clone = serde_mock.clone();
        match GameEnv::<SerdeMock, SerdeMock>::reply(&mut game_env.unwrap(), serde_mock) {
            Ok(_) => println!("Client: reply ok"),
            Err(error) => println!("Client: reply error: {}", error),
        }
        let res = receiver.recv().unwrap();
        assert_eq!(res, another_serde_mock_clone);
    }

    #[test]
    fn test_response() {
        let port = 9002;
        let serde_mock = SerdeMock::new("Hello");
        let serde_mock_clone = serde_mock.clone();

        thread::spawn(move || {
            let server_mock = TcpListener::bind(format!("127.0.0.1:{}", port)).unwrap();
            match server_mock.accept() {
                Ok((mut stream, _)) => {
                    let serialized_mock = serde_json::to_string(&serde_mock).unwrap();
                    println!(
                        "Server: serialized struct: {}, length: {}",
                        serialized_mock,
                        serialized_mock.len()
                    );

                    let length = u32::try_from(serialized_mock.len()).unwrap().to_be_bytes();
                    stream.write(&length).unwrap();
                    stream.write(serialized_mock.as_bytes()).unwrap();
                }
                Err(e) => println!("Server: couldn't get client: {:?}", e),
            }
        });

        thread::sleep(Duration::from_millis(1));

        let mut game_env = TablutCompetionEnv::from(format!("127.0.0.1:{}", port));
        match &mut game_env {
            Ok(_) => {
                println!("Client: socket ok");
            }
            Err(error) => println!("Client: socket error: {}", error),
        }

        let response = GameEnv::<SerdeMock, SerdeMock>::response(&mut game_env.unwrap()).unwrap();
        assert_eq!(response, serde_mock_clone);
    }
}
