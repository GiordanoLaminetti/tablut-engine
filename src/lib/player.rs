pub mod mcts_player;
pub mod random_player;

use serde::Serialize;

/* PLAYERS */

/// players in the game for a zero-sum 2-players game
#[derive(Serialize, Debug, Clone, Copy, PartialEq)]
pub enum Players {
    Me,
    Other,
}

impl Players {
    fn next_player(&self) -> Self {
        match self {
            Players::Me => Players::Other,
            Players::Other => Players::Me,
        }
    }
}

/* GAME RESULT */
/// Designates the outcome of a Game match
/// Either a player wins or the match ends in stalemate
#[derive(Serialize)]
pub enum GameResult {
    Stalemate,
    Wins(Players),
}

/* PLAYER */

/// Player interface, a player knows what action
/// associate to the current game state.
pub trait Player<S, A> {
    /// next_move returns next action to apply to
    /// the game. It returns None if game is ended
    /// or if from the current state there aren't
    /// valid actions
    fn next_move(&mut self, state: S) -> Option<A>;
}

/* REPORTER */
/// Reporter trait, the player can implement
/// this trait if it should give information about
/// the gameplay and its computation
pub trait Reporter<R>
where
    R: Serialize,
{
    /// return a vector of reports
    fn report(&mut self) -> Option<R>;
}

/* GAME DEFINITION */

/// Game environment which defines what actions player can
/// do, what actions other player can do and what happens
/// applying an action to a state
pub trait Game<S, A> {
    type Actions: Iterator<Item = A>;

    /// applies an action to  a state and returns new state,
    /// *given action must be always applicable to given state
    /// cause given state will be taken by `my_actions` or
    /// `other_actions`*.
    fn apply(&self, state: &S, action: &A) -> S;

    /// returns an iterator of Actions from current state, this
    /// function is called for every state of the player
    fn my_actions(&self, state: &S) -> Self::Actions;

    /// returns an iterator of Actions from current state for
    /// the other player
    fn other_actions(&self, state: &S) -> Self::Actions;

    /// returns None if state is not end, or Some with the player
    /// who wins
    fn terminal_state(&self, state: &S) -> Option<GameResult>;
}
