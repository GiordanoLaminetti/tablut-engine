pub mod tablut_competition_env;

use std::error;
use std::fmt;
use std::io;

/// Defines environment for a game, a player can
/// send an action and receive response.  This trait
/// is used in order to interface the player with an
/// extern environment
pub trait GameEnv<A, B> {
    /// send an action to the environment, such action
    /// can be of any serializable type
    fn reply(&mut self, a: A) -> Result<(), Error>;

    /// wait for a response from the environment

    fn response(&mut self) -> Result<B, Error>;
}

// Error

#[derive(Debug)]
pub enum Error {
    IoError(io::Error),
    Error(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Conversion Error")
    }
}

impl error::Error for Error {}

impl From<io::Error> for Error {
    fn from(cause: io::Error) -> Error {
        Error::IoError(cause)
    }
}
