use crate::games as game;
use ndarray::array;
use serde::Deserialize;
use serde::Serialize;
#[derive(Serialize, Debug)]
pub struct MoveMessage {
    to: String,
    from: String,
}

impl MoveMessage {
    pub fn init(mov: game::tablut::Move) -> MoveMessage {
        let letter = vec!["A", "B", "C", "D", "E", "F", "G", "H", "I"];
        let rto = letter[mov.to.0].to_string() + &mov.to.1.to_string();
        let rfrom = letter[mov.from.0].to_string() + &mov.from.1.to_string();
        MoveMessage {
            to: rto,
            from: rfrom,
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct TablutStateMessage {
    pub board: [[String; 9]; 9],
    pub turn: String,
}

impl TablutStateMessage {
    pub fn convertState(&mut self) -> game::tablut::Board {
        let mut res = game::tablut::Board::init();
        let e = game::tablut::Pawn::Empty;
        let mut tmp = array![
            [e, e, e, e, e, e, e, e, e],
            [e, e, e, e, e, e, e, e, e],
            [e, e, e, e, e, e, e, e, e],
            [e, e, e, e, e, e, e, e, e],
            [e, e, e, e, e, e, e, e, e],
            [e, e, e, e, e, e, e, e, e],
            [e, e, e, e, e, e, e, e, e],
            [e, e, e, e, e, e, e, e, e],
            [e, e, e, e, e, e, e, e, e],
        ];
        for (i, row) in self.board.iter_mut().enumerate() {
            for (y, col) in row.iter_mut().enumerate() {
                if col == "BLACK" {
                    tmp[[i, y]] = game::tablut::Pawn::Black;
                } else if col == "WHITE" {
                    tmp[[i, y]] = game::tablut::Pawn::White;
                } else if col == "KING" {
                    tmp[[i, y]] = game::tablut::Pawn::King;
                }
            }
        }
        res.set(tmp);
        res
    }
}

//test

#[cfg(test)]
mod tests {
    use crate::games::tablut;
    use serde::{Deserialize, Serialize};
    #[test]
    fn test_serialize_move() {
        let a = Move {
            to: (2, 3),
            from: (3, 2),
        };
        let b = MoveMessage::init(a);
        let serialized = serde_json::to_string(&b).unwrap();
        assert_eq!("{\"to\":\"C3\",\"from\":\"D2\"}", serialized)
    }

    #[test]
    fn test_deserialize_tablut() {
        let mut deserializzed : TablutStateMessage = serde_json::from_str("{\"board\":[[\"EMPTY\",\"EMPTY\",\"EMPTY\",\"BLACK\",\"BLACK\",\"BLACK\",\"EMPTY\",\"EMPTY\",\"EMPTY\"],[\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"BLACK\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\"],[\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"WHITE\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\"],[\"BLACK\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"WHITE\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"BLACK\"],[\"BLACK\",\"BLACK\",\"WHITE\",\"WHITE\",\"KING\",\"WHITE\",\"WHITE\",\"BLACK\",\"BLACK\"],[\"BLACK\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"WHITE\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"BLACK\"],[\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"WHITE\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\"],[\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"BLACK\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\"],[\"EMPTY\",\"EMPTY\",\"EMPTY\",\"BLACK\",\"BLACK\",\"BLACK\",\"EMPTY\",\"EMPTY\",\"EMPTY\"]],\"turn\":\"WHITE\"}").unwrap();
        let table = deserializzed.convertState();
        let mut d = Board::init();
        assert_eq!(d, table);
    }

}
