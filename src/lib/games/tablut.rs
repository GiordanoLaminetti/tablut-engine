mod test_apply;
mod test_moves;
mod test_valid;
mod test_to_eat;
pub mod players;
pub mod game_board;

use serde::ser::{Serialize, SerializeStruct, Serializer};
use serde::Deserialize;
use std::error;
use std::fmt;

/* PLAYER DEFINITION */

/// which player who owns the next move
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Player {
    White,
    Black,
}

/* ERROR DEFINITION */

/// Tablut board errors
#[derive(PartialEq, Copy, Clone, Debug)]
pub enum Error {
    MoveOutBoard(Move),
    MoveToNotEmpty(Move),
    MoveEmpty(Pos),
    MoveToThrone,
    MoveToBlackCamp(Move),
    MoveStationary(Pos),
    MoveDiagonal(Move),
    MoveAcrossCamp(Move),
    MoveAcrossThrone(Move),
    MoveAcrossNotEmpty(Move),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, " - Moving error")
    }
}

impl error::Error for Error {}

/* MOVE DEFINITION */

/// Simple alias for a tuple for board indexing
pub type Pos = (u8, u8);

/// A Move on the game board
#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Move {
    pub from: Pos,
    pub to: Pos,
}

impl Serialize for Move {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let letter = vec!["A", "B", "C", "D", "E", "F", "G", "H", "I"];
        let mut state = serializer.serialize_struct("Move", 2)?;
        let rto = letter[self.to.1 as usize].to_string() + &(self.to.0 + 1).to_string();
        let rfrom = letter[self.from.1 as usize].to_string() + &(self.from.0 + 1).to_string();
        state.serialize_field("to", &rto)?;
        state.serialize_field("from", &rfrom)?;
        state.end()
    }
}

/* BOARD DEFINITION */

/// Tablut 9x9 Game Board
#[derive(Clone, PartialEq)]
pub struct Board {
    pub white: Vec<u8>,
    pub black: Vec<u8>,
    pub king: u8,
}

impl fmt::Debug for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut table = [[' '; 9]; 9];
        for (r, c) in self.all_whites(false) {
            table[r as usize][c as usize] = 'w';
        }
        for (r, c) in self.all_blacks() {
            table[r as usize][c as usize] = 'b';
        }
        if let Some((r, c)) = self.king_pos() {
            table[r as usize][c as usize] = 'K';
        }

        for r in 0 .. 9 {
            for c in 0 .. 9 {
                write!(f, "{}", table[r][c]);
            }
            write!(f, "\n");
        }

        Ok(())
    }
}

/* UTILITY FUNCTION */

// convert a integer in (row, col)
fn coordinates(v: u8) -> Pos {
    let r = v / 10;
    let c = v - (r * 10);
    (r, c)
}

fn encode((r, c): Pos) -> u8 {
    r * 10 + c
}

/* BOARD METHODS */

impl Board {
    /// returns the tablut board initialized for the start of the game
    pub fn init() -> Board {
        let white = vec![24, 34, 54, 64, 42, 43, 45, 46];
        let king = 44;
        let black = vec![
            30, 40, 50, 41, 03, 04, 05, 14, 83, 84, 85, 74, 38, 48, 58, 47,
        ];
        Board { white, black, king }
    }

    pub fn set_v(&mut self, black: Vec<u8>, white: Vec<u8>, king: u8) {
        self.black = black;
        self.white = white;
        self.king = king;
    }

    pub fn set(&mut self, mut board: [[&str; 9]; 9]) {
        self.black = Vec::new();
        self.white = Vec::new();
        self.king = 100;

        for (i, row) in board.iter_mut().enumerate() {
            for (y, col) in row.iter_mut().enumerate() {
                if *col == "BLACK" {
                    self.black.push((i * 10 + y) as u8); 
                } else if *col == "WHITE" {
                    self.white.push((i * 10 + y) as u8);
                } else if *col == "KING" {
                    self.king = (i * 10 + y) as u8;
                }
            }
        }
    }

    // positions where a pawn is blocked on side
    fn capture_positions(&self, p : Player) -> Vec<u8> {
        let mut positions = Vec::new();
        
        // castle
        positions.push(44);

        // camps
        positions.append(&mut vec![30, 50, 41, 3, 5, 14, 38, 47, 58, 74, 83, 85]);

        // other playes elements
        match p {
            Player::White => positions.append(&mut self.black.clone()),
            Player::Black => positions.append(&mut self.white.clone()),
        }

        if self.king != 100 {
            positions.push(self.king);
        }

        positions
    }

    // return true if the pawn must be eated
    fn to_eat(&self, pos: u8) -> bool {

        let player = if self.white.contains(&pos) || self.king == pos {
            Player::White
        } else if self.black.contains(&pos) {
            Player::Black
        } else {
            return false;
        };

        let capture_positions = self.capture_positions(player);

        // check king near castle
        let near_castle = vec![44, 43, 34, 45, 54];
        if self.king == pos && near_castle.contains(&pos) {
            let near_king = vec![pos - 10, pos + 10, pos - 1, pos + 1];
            let capture_near_king: Vec<&u8> = capture_positions.iter().filter(|v| near_king.contains(v)).collect();
            if capture_near_king.len() == 4 { return true } else { return false }
        }
        
        // check normal pawn or king far from castle
        if pos >= 10 {
            let (up, down) = (pos - 10, pos + 10);
            if capture_positions.contains(&up) && capture_positions.contains(&down) { return true }
        }
        if pos != 0 {
            let (left, right) = (pos - 1, pos + 1);
            if capture_positions.contains(&left) && capture_positions.contains(&right) { return true }
        }

        false
    }

    // function to eat a piece
    fn eat_(&mut self, p: u8) -> Result<(), Error> {

        if self.to_eat(p) {
            self.white = self.white.clone().into_iter().filter(|v| *v != p).collect();
            self.black = self.black.clone().into_iter().filter(|v| *v != p).collect();

            if p == self.king { self.king = 100 }
        }

        Ok(())
    }

    /// checks if a move can eat some pawn
    pub fn can_eat(&self, m: Move) -> bool {
        let mut result = false;
        let mut board_copy = self.clone();
        if board_copy.apply_without_eat(m).is_ok() {
            let around = Board::around(encode(m.to));
            let to_eat: Vec<u8> = around.into_iter()
                .filter(|p| board_copy.to_eat(*p))
                .collect();
            result = !to_eat.is_empty();
        };
        result
    }

    /// checks if king can be eaten with passed Move
    pub fn can_eat_king(&self, m: Move) -> bool {

        // if I'm moving king it cannot be eaten
        if self.king == encode(m.from) {
            return false;
        }

        // if move does not reach king..
        let around_king = Board::around(self.king);
        if !around_king.contains(&encode(m.to)) {
            return false
        }

        let mut board_copy = self.clone();
        if board_copy.apply_without_eat(m).is_ok() {
            return board_copy.to_eat(board_copy.king);
        };

        false
    }

    /// if king can be moved on board side returns correct move
    pub fn king_win_move(&self) -> Option<Move> {

        let (kr, kc) = coordinates(self.king);
        let mut moves: Vec<Move> = vec![(kr, 0), (kr, 8), (0, kc), (8, kc)]
            .into_iter()
            .map(|p| Move{ from: (kr, kc), to: p })
            .filter(|m| self.valid(m).is_ok() )
            .collect();

        moves.pop()
    }

    /// returns White | Black if passed pos is a black or
    /// white pawn on board
    pub fn pawn_owner(&self, pos: Pos) -> Option<Player> {

        if (self.king == encode(pos) && self.king != 100) || self.white.contains(&encode(pos)) {
            return Some(Player::White);
        }
        if self.black.contains(&encode(pos)) {
            return Some(Player::Black);
        }

        None
    }

    // returns positions around passed encoded position
    fn around(p: u8) -> Vec<u8> {
        let movi = p as i8;
        vec![movi - 1, movi + 1, movi - 10, movi + 10].into_iter()
            .filter(|v| *v >= 0 )
            .map(|v| v as u8)
            .collect()
    }

    fn apply_without_eat(&mut self, m: Move) -> Result<(), Error> {
        self.valid(&m)?;

        let movf = encode(m.from);
        let movt = encode(m.to);

        if let Some(i) = self.white.iter().position(|v| *v == movf) {
            self.white.remove(i);
            self.white.push(movt);
        } else if let Some(i) = self.black.iter().position(|v| *v == movf) {
            self.black.remove(i);
            self.black.push(movt);
        } else if self.king == movf {
            self.king = movt;
        }

        Ok(())
    }

    /// apply a move to the board
    pub fn apply_(&mut self, m: Move) -> Result<(), Error> {
        self.apply_without_eat(m)?;
        
        let movt = encode(m.to);
        let around = Board::around(movt);
        for pos in around {
            self.eat_(pos)?;
        }

        Ok(())
    }

    /// apply a move to the board
    pub fn apply(mut self, m: Move) -> Result<Board, Error> {
        self.apply_(m)?;
        Ok(self)
    }

    /// apply multiple moves to the board
    pub fn applym_<I>(&mut self, moves: I) -> Result<(), Error>
    where
        I: Iterator<Item = Move>,
    {
        for m in moves {
            self.apply_(m)?;
        }
        Ok(())
    }

    /// apply a sequence of moves to the board
    pub fn applym<I>(mut self, moves: I) -> Result<Board, Error>
    where
        I: Iterator<Item = Move>,
    {
        for m in moves {
            self.apply_(m)?;
        }
        Ok(self)
    }

    /// returns all white positions
    pub fn all_whites<'a>(&'a self, king: bool) -> impl Iterator<Item = Pos> + 'a {
        let mut val = self.white.to_vec();
        if king && self.king != 100 {
            val.push(self.king);
        }
        val.into_iter().map(|d| coordinates(d))
    }

    /// returns all black positions
    pub fn all_blacks<'a>(&'a self) -> impl Iterator<Item = Pos> + 'a {
        let ret = self.black.to_vec();
        ret.into_iter().map(|d| coordinates(d))
    }

    /// returns king position
    pub fn king_pos(&self) -> Option<Pos> {
        match self.king {
            100 => None,
            _   => Some(coordinates(self.king)),
        }
    }

    /// checks if a Move is valid in current board
    pub fn valid(&self, m: &Move) -> Result<(), Error> {
        let (xf, yf) = m.from;
        let (xt, yt) = m.to;
        let posf = encode(m.from);
        let post = encode(m.to);

        // checks board limits
        if xt > 8 || yt > 8 || xf > 8 || yf > 8 {
            return Err(Error::MoveOutBoard(*m));
        }

        // checks not move empty
        if !self.black.contains(&posf) && !self.white.contains(&posf) && self.king != posf {
            return Err(Error::MoveEmpty(m.from));
        }

        // checks target Pos
        if self.black.contains(&post) || self.white.contains(&post) || self.king == post {
            return Err(Error::MoveToNotEmpty(*m));
        }

        // check not going into black camps
        let all_black_camp = vec![
            (0, 3),
            (0, 4),
            (0, 5),
            (1, 4),
            (3, 0),
            (4, 0),
            (5, 0),
            (4, 1),
            (3, 8),
            (4, 8),
            (5, 8),
            (4, 7),
            (8, 3),
            (8, 4),
            (8, 5),
            (7, 4),
        ];
        let black_camp_top = &all_black_camp[0..4];
        let black_camp_bottom = &all_black_camp[12..16];
        let black_camp_left = &all_black_camp[4..8];
        let black_camp_right = &all_black_camp[8..12];

        if !black_camp_top.contains(&m.from) && black_camp_top.contains(&m.to) {
            return Err(Error::MoveToBlackCamp(*m));
        }
        if !black_camp_bottom.contains(&m.from) && black_camp_bottom.contains(&m.to) {
            return Err(Error::MoveToBlackCamp(*m));
        }
        if !black_camp_left.contains(&m.from) && black_camp_left.contains(&m.to) {
            return Err(Error::MoveToBlackCamp(*m));
        }
        if !black_camp_right.contains(&m.from) && black_camp_right.contains(&m.to) {
            return Err(Error::MoveToBlackCamp(*m));
        }

        // checks not stationary
        if m.from == m.to {
            return Err(Error::MoveStationary(m.from));
        }

        // checks not going to throne
        if m.to == (4, 4) {
            return Err(Error::MoveToThrone);
        }

        // checks not diagonal move
        if !(xf == xt || yf == yt) {
            return Err(Error::MoveDiagonal(*m));
        }

        // checks not to step over pawns or throne or black camps
        let diffy = if yf > yt { yf - yt } else { yt - yf };
        let diffx = if xf > xt { xf - xt } else { xt - xf };
        let to_steps = |i| {
            if diffx == 0 && yf > yt {
                (xf, yf - i)
            } else if diffx == 0 && yf < yt {
                (xf, yf + i)
            } else if xf > xt {
                (xf - i, yf)
            } else {
                (xf + i, yf)
            }
        };

        let steps = if diffx == 0 {
            (1..diffy + 1)
        } else {
            (1..diffx + 1)
        };
        let steps: Vec<Pos> = steps.map(to_steps).filter(|m| *m != (xt, yt)).collect();
        for step in steps {
            if all_black_camp[..].contains(&step) && !all_black_camp.contains(&m.from) {
                return Err(Error::MoveAcrossCamp(*m));
            }
            if step == (4, 4) {
                return Err(Error::MoveAcrossThrone(*m));
            }
            let (xs, ys) = step;
            let pos = (xs * 10 + ys) as u8;
            if self.black.contains(&(pos)) || self.white.contains(&(pos)) || self.king == pos {
                return Err(Error::MoveAcrossNotEmpty(*m));
            }
        }

        Ok(())
    }

    /// returns all possible moves for white player
    pub fn white_moves<'a>(&'a self) -> impl Iterator<Item = Move> + 'a {
        // generate moves from a pos
        let generate = |pos| {
            let (x, y) = pos;
            (0..9)
                .map(move |v| Move {
                    from: pos,
                    to: (v, y),
                })
                .chain((0..9).map(move |v| Move {
                    from: pos,
                    to: (x, v),
                }))
        };

        // generate all moves for all white pos
        self.all_whites(true)
            .map(move |p| generate(p))
            .flatten()
            .filter(move |m| m.from != m.to)
            .filter(move |m| self.valid(m).is_ok())
    }

    /// returns all possible moves for black player
    pub fn black_moves<'a>(&'a self) -> impl Iterator<Item = Move> + 'a {
        // all whites moves
        let generate = |pos| {
            let (x, y) = pos;
            (0..9)
                .map(move |v| Move {
                    from: pos,
                    to: (v, y),
                })
                .chain((0..9).map(move |v| Move {
                    from: pos,
                    to: (x, v),
                }))
        };

        // generates moves for pos
        self.all_blacks()
            .map(move |p| generate(p))
            .flatten()
            .filter(move |m| m.from != m.to)
            .filter(move |m| self.valid(m).is_ok())
    }

    /// returns None if game is not yet over, otherwise the winner.
    pub fn winner(&self) -> Option<Player> {

        // black wins
        let no_king = self.king == 100;
        if no_king {
            return Some(Player::Black);
        }

        // white wins
        //check if a king is in 0 or 8 line or 0 or 8 columns
        let king_win = self.king % 10 == 0 || self.king < 10 || self.king % 10 == 8 || self.king >= 80;

        if king_win {
            return Some(Player::White);
        }

        None
    }

    /* Associated Functions */

    pub fn to_csv(a :Board, prob: f64)-> String {
        let mut tab1:String="".to_string();
        let prob_s = prob.to_string();
        for i in 0..9 {
            for k in 0..9 {
                let pos = (i*10+k) as u8;
                match pos {
                    p if a.white.contains(&p) => tab1 += &"125,",
                    p if a.black.contains(&p) => tab1 += &"63,",
                    p if a.king == p => tab1 += &"255,",
                    _ => tab1 += &"0,",
                }
            }
        }
        return tab1 + &prob_s + &"\n";
    }

    /// compute 3 clockwise rotations of board
    pub fn rotations(board: &Board) -> Vec<Board> {

        let rotate = |p: &u8| {
            let (r, c) = coordinates(*p);
            encode((c, 8 - r))
        };

        let mut current_rotation: Board = board.clone();
        let mut rotations = Vec::new();

        for _ in 0 .. 3 {
            let white_rotation: Vec<u8> = current_rotation.white.iter().map(rotate).collect();
            let black_rotation: Vec<u8> = current_rotation.white.iter().map(rotate).collect();
            let king_rotation = rotate(&current_rotation.king);

            let rotated_board = Board {
                white: white_rotation,
                black: black_rotation,
                king: king_rotation,
            };

            current_rotation = rotated_board.clone();
            rotations.push(rotated_board);
        }

        rotations
    }

    /// computes 2 vertical and horizontal reflections of Board
    pub fn flips(board: &Board) -> Vec<Board> {

        // vertical flip
        let vflip = |p: &u8| {
            let (r, c) = coordinates(*p);
            encode((r, 8 - c))
        };

        let white = board.white.iter().map(vflip).collect();
        let black = board.black.iter().map(vflip).collect();
        let king = vflip(&board.king);

        let vflip_board = Board { white, black, king };

        // horizontal flip
        let hflip = |p: &u8| {
            let (r, c) = coordinates(*p);
            encode((r - 8, c))
        };

        let white = board.white.iter().map(hflip).collect();
        let black = board.black.iter().map(hflip).collect();
        let king = hflip(&board.king);

        let hflip_board = Board { white, black, king };

        vec![ vflip_board, hflip_board ]
    }

}

#[derive(Deserialize, Debug)]
pub struct TablutStateMessage {
    pub board: [[String; 9]; 9],
    pub turn: String,
}

impl TablutStateMessage {
    pub fn convert_state(&mut self) -> Board {
        let mut res = Board::init();
        let mut black: Vec<u8> = Vec::new();
        let mut white: Vec<u8> = Vec::new();
        let mut king: u8 = 44;
        for (i, row) in self.board.iter_mut().enumerate() {
            for (y, col) in row.iter_mut().enumerate() {
                if col == "BLACK" {
                    black.push((i * 10 + y) as u8)
                } else if col == "WHITE" {
                    white.push((i * 10 + y) as u8)
                } else if col == "KING" {
                    king = (i * 10 + y) as u8
                }
            }
        }
        res.set_v(black, white, king);
        res
    }
}
