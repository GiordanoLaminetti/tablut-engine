#[cfg(test)]
use crate::games::tablut::{Board, Error, Move, Player};

#[test]
fn to_eat_white() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, e, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, e, e, b, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, b, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, b, b, e, e, e],
    ]);

    assert!(tablut.to_eat(43));
}

#[test]
fn to_eat_black() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, w, e, e, e, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, w, e, e, e, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    assert!(tablut.to_eat(41));
}

#[test]
fn to_eat_free_king() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, e, e, e, e, e],
        [e, e, e, e, e, b, k, b, e],
        [b, e, e, e, e, e, e, e, e],
        [b, b, w, w, e, e, w, b, b],
        [b, e, e, e, e, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    assert!(tablut.to_eat(26));
}

#[test]
fn to_eat_king_in_castle() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, e, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, b],
        [b, b, w, b, k, b, e, e, b],
        [b, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, b, b, e, e, e],
    ]);

    assert!(tablut.to_eat(44));
}

#[test]
fn to_eat_king_near_castle() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, e, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, e, b, e, e, b],
        [b, b, w, b, e, k, b, e, b],
        [b, e, e, e, e, b, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, b, b, e, e, e],
    ]);

    assert!(tablut.to_eat(45));
}
