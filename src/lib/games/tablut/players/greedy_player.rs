// greedy players

use crate::player::*;
use crate::player::Player;
use crate::player::mcts_player::{SimulationPlayer, SimulationGame};
use super::super::*; // tablut
use rand::Rng;

#[derive(Clone)]
pub struct GreedyWhitePlayer {
    game: game_board::BoardWhiteGame,
    prob: u8,
}

impl GreedyWhitePlayer {
    pub fn new_prob(prob: u8) -> Self
    {
        GreedyWhitePlayer { game: game_board::BoardWhiteGame::new() , prob}
    }
    pub fn new() -> Self
    {
        GreedyWhitePlayer { game: game_board::BoardWhiteGame::new() , prob: 0}
    }
}

impl Player<Board, Move> for GreedyWhitePlayer
{
    fn next_move(&mut self, state: Board) -> Option<Move> {
        let mut rand_gen = rand::thread_rng();

        // check can king win
        if let Some(win_move) = state.king_win_move() {
            return Some(win_move);
        }

        // check can eat something .. but do it at 50%
        let mut all_moves: Vec<Move> = self.game.my_actions(&state).collect();
        // verify that king can be eaten in the next move
        /*
        let all_moves_king_can_be_eaten: Vec<Move> = all_moves.iter()
            .filter(|m| {
                let next_tablut = self.game.apply(&state, m);
                self.game.other_actions(&next_tablut).find(|m| next_tablut.can_eat_king(*m)).is_some()
            })
            .map(|m| *m)
            .collect();
        all_moves = all_moves.into_iter().filter(|m| ! all_moves_king_can_be_eaten.contains(&m)).collect();
        */
        let all_eat_moves: Vec<Move> = self.game.my_actions(&state)
            .filter(|m| state.can_eat(*m))
            .collect();
        if ! all_eat_moves.is_empty() && rand_gen.gen_range(1, 101) <= self.prob  {
            all_moves = all_eat_moves;
        }

        match all_moves.len() {
            0 => None,
            len => {
                let num: usize = rand_gen.gen_range(0, len);
                let choice = all_moves.remove(num);
                Some(choice)
            }
        }
    }
}
impl<G> SimulationPlayer<G, Board, Move> for GreedyWhitePlayer
where
    G: Game<Board, Move>,
{
    fn next_move(&mut self, game: &SimulationGame<G>, state: &Board) -> Option<Move> {
        let mut rand_gen = rand::thread_rng();

        // check can king win
        if let Some(win_move) = state.king_win_move() {
            return Some(win_move);
        }


        // check can eat someting  .. or random
        let mut all_moves: Vec<Move> = game.my_actions(&state).collect();
        // verify that king can be eaten in the next move
        /*
        let all_moves_king_can_be_eaten: Vec<Move> = all_moves.iter()
            .filter(|m| {
                let next_tablut = game.apply(&state, m);
                game.other_actions(&next_tablut).find(|m| next_tablut.can_eat_king(*m)).is_some()
            })
            .map(|m| *m)
            .collect();

        all_moves = all_moves.into_iter().filter(|m| ! all_moves_king_can_be_eaten.contains(&m)).collect(); */


        let all_eat_moves: Vec<Move> = game.my_actions(&state)
            .filter(|m| state.can_eat(*m))
            .collect();
            
        if ! all_eat_moves.is_empty() && rand_gen.gen_range(1, 101) <= self.prob {
            all_moves = all_eat_moves;
        }

        match all_moves.len() {
            0 => None,
            len => {
                let num: usize = rand_gen.gen_range(0, len);
                let choice = all_moves.remove(num);
                Some(choice)
            }
        }
    }
}

#[derive(Clone)]
pub struct GreedyBlackPlayer {
    game: game_board::BoardBlackGame,
    prob: u8,
}

impl GreedyBlackPlayer {
    pub fn new_prob(prob: u8) -> Self
    {
        GreedyBlackPlayer { game: game_board::BoardBlackGame::new(), prob }
    }
    pub fn new() -> Self
    {
        GreedyBlackPlayer { game: game_board::BoardBlackGame::new(), prob : 0 }
    }
}

fn find_between(win_move: Move, v: u8, row: bool) -> Box<dyn Iterator<Item=u8>> {
    if v == 0 {
        match row{
            true  => return Box::new(vec![win_move.from.0].into_iter().cycle()),
            false => return Box::new(vec![win_move.from.1].into_iter().cycle()),
        }
        
    } else {
        match row{
            true =>  match win_move.from.0 > win_move.to.0 {
                true => return Box::new(win_move.to.0 .. win_move.from.0),
                false => return Box::new(win_move.from.0 .. win_move.to.0)
            },
            false => match win_move.from.1 > win_move.to.1 {
                true => return Box::new(win_move.to.1 .. win_move.from.1),
                false => return Box::new(win_move.from.1 .. win_move.to.1)
            },
        }
        
    }
}

impl Player<Board, Move> for GreedyBlackPlayer
{
     fn next_move(&mut self, state: Board) -> Option<Move> {
        let mut rand_gen = rand::thread_rng();

        let mut all_moves: Vec<Move> = self.game.my_actions(&state).collect();

        // check can eat king
        let mut eat_king_moves: Vec<&Move> = all_moves
            .iter()
            .filter(|m| state.can_eat_king(**m))
            .collect();
        if !eat_king_moves.is_empty() {
            let move_ref: &Move = eat_king_moves.pop().unwrap();
            return Some(*move_ref);
        }

        // check must block king winner
        /* 
        if let Some(win_move) = state.king_win_move() {
            let (dr, dc) = ((win_move.from.0 as i8 - win_move.to.0 as i8).abs() as u8, (win_move.from.1 as i8 - win_move.to.1 as i8).abs() as u8);
            let drs = find_between(win_move, dr,true);
            let dcs = find_between(win_move, dc,false);
            let all_to: Vec<(u8, u8)> = drs.zip(dcs).collect();

            let result = all_moves.iter().find(|m| all_to.contains(&m.to)).map(|v| v.clone());
            if result.is_some() {
                return result;
            }
        }
        */
        
        // check can eat something .. or random
        let all_eat_moves: Vec<Move> = self.game.my_actions(&state)
            .filter(|m| state.can_eat(*m))
            .collect();
        if ! all_eat_moves.is_empty() && rand_gen.gen_range(1, 101) <= self.prob{
            all_moves = all_eat_moves;
        }

        match all_moves.len() {
            0 => None,
            len => {
                let num: usize = rand_gen.gen_range(0, len);
                let choice = all_moves.remove(num);
                Some(choice)
            }
        }
    }
}

impl<G> SimulationPlayer<G, Board, Move> for GreedyBlackPlayer
where
    G: Game<Board, Move>,
{
    fn next_move(&mut self, game: &SimulationGame<G>, state: &Board) -> Option<Move> {
        let mut rand_gen = rand::thread_rng();

        let mut all_moves: Vec<Move> = game.my_actions(&state).collect();

        // check can eat king
        let mut eat_king_moves: Vec<&Move> = all_moves
            .iter()
            .filter(|m| state.can_eat_king(**m))
            .collect();
        if !eat_king_moves.is_empty() {
            let move_ref: &Move = eat_king_moves.pop().unwrap();
            return Some(*move_ref);
        }

        // check must block king winner
        /*
        if let Some(win_move) = state.king_win_move() {
            let (dr, dc) = ((win_move.from.0 as i8 - win_move.to.0 as i8).abs() as u8, (win_move.from.1 as i8 - win_move.to.1 as i8).abs() as u8);
            let drs = find_between(win_move, dr,true);
            let dcs = find_between(win_move, dc,false);
            let all_to: Vec<(u8, u8)> = drs.zip(dcs).collect();

            let result = all_moves.iter().find(|m| all_to.contains(&m.to)).map(|v| v.clone());
            if result.is_some() {
                return result;
            }
        }
        */
        // check can eat something .. or random
        let all_eat_moves: Vec<Move> = game.my_actions(&state)
            .filter(|m| state.can_eat(*m))
            .collect();
        if ! all_eat_moves.is_empty() && rand_gen.gen_range(1, 101) <= self.prob {
            all_moves = all_eat_moves;
        }

        match all_moves.len() {
            0 => None,
            len => {
                let num: usize = rand_gen.gen_range(0, len);
                let choice = all_moves.remove(num);
                Some(choice)
            }
        }

    }
}
