use crate::games::tablut;
use crate::games::tablut::{Board, Move};
use crate::player::Player;
use std::env;
use std::error;
use tensorflow::Graph;
use tensorflow::Operation;
use tensorflow::Session;
use tensorflow::SessionOptions;
use tensorflow::SessionRunArgs;
use tensorflow::Tensor;

pub struct CnnPlayer {
    player: tablut::Player,
    input: Operation,
    ratio: Operation,
    session: Session,
}

fn convert_board(board: &Board) -> Tensor<f32> {
    let mut from = [0.; 81];
    for (r, c) in board.all_whites(false) {
        from[(r * 9 + c) as usize] = 125.;
    }
    for (r, c) in board.all_blacks() {
        from[(r * 9 + c) as usize] = 63.;
    }
    if let Some(king_pos) = board.king_pos() {
        from[(king_pos.0 * 9 + king_pos.1) as usize] = 255.;
    }

    let mut x: Tensor<f32> = Tensor::new(&[1, 9, 9, 1]);
    x = x.with_values(&from).unwrap();
    x
}

impl CnnPlayer {
    pub fn new(player: tablut::Player, export_dir: &str) -> Result<Self, Box<dyn error::Error>> {
        // disable tensorflow prints
        env::set_var("TF_CPP_MIN_LOG_LEVEL", "3");

        // load graph and input/output nodes
        let mut graph = Graph::new();

        let session =
            Session::from_saved_model(&SessionOptions::new(), &["serve"], &mut graph, export_dir)?;

        Ok(CnnPlayer {
            player,
            input: graph.operation_by_name_required("X")?,
            ratio: graph.operation_by_name_required("tablut_cnn/ratio/BiasAdd")?,
            session,
        })
    }

    pub fn into_predict_ratio(self) -> impl Fn(&Board) -> (u32, u32) {
        move |board| {
            let boards = convert_board(board);

            let mut comp_step = SessionRunArgs::new();
            comp_step.add_feed(&self.input, 0, &boards);
            let ratio_ix = comp_step.request_fetch(&self.ratio, 0);
            self.session.run(&mut comp_step).unwrap();

            let rs: f32 = comp_step.fetch(ratio_ix).unwrap()[0];
            let mut rs = rs * 100.;

            if rs > 100. {
                rs = 100.;
            }

            if rs < 0. {
                rs = 0.;
            }

            (rs as u32, 100)
        }
    }
}

impl Player<Board, Move> for CnnPlayer {
    fn next_move(&mut self, state: tablut::Board) -> Option<tablut::Move> {
        let mut max = 0;
        let all_actions: Vec<Move> = match self.player {
            tablut::Player::White => state.white_moves().collect(),
            tablut::Player::Black => state.black_moves().collect(),
        };
        let mut max_action: tablut::Move = all_actions[0];

        for action in all_actions {
            let boards = convert_board(&state);
            let mut comp_step = SessionRunArgs::new();
            comp_step.add_feed(&self.input, 0, &boards);
            let ratio_ix = comp_step.request_fetch(&self.ratio, 0);
            self.session.run(&mut comp_step).unwrap();

            let rs: f32 = comp_step.fetch(ratio_ix).unwrap()[0];
            let rs = (rs * 100.) as u32;
            if rs > max {
                max = rs;
                max_action = action;
            }
        }
        Some(max_action)
    }
}
