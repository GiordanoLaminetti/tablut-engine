// Boards for play

use crate::player::*;
use crate::player::GameResult;
use super::*; // tablut
use crate::games::tablut::Player as PlayerRole;

#[derive(Clone)]
pub struct BoardWhiteGame;

impl BoardWhiteGame {

    pub fn new() -> Self {
        BoardWhiteGame
    }

}

pub struct BoardGameIter(Vec<Move>);

impl Iterator for BoardGameIter {
    type Item = Move;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.pop()
    }
}

impl Game<Board, Move> for BoardWhiteGame {
    type Actions = BoardGameIter;

    fn apply(&self, state: &Board, action: &Move) -> Board {
        let mut state_clone: Board = (*state).clone();
        state_clone.apply_(*action);
        state_clone
    }

    fn my_actions(&self, state: &Board) -> Self::Actions {
        BoardGameIter(state.white_moves().collect())
    }

    fn other_actions(&self, state: &Board) -> Self::Actions {
        BoardGameIter(state.black_moves().collect())
    }

    fn terminal_state(&self, state: &Board) -> Option<GameResult> {
        let game_result = state.winner();

        if let None = game_result {
            return None;
        } else {
            match game_result.unwrap() {
                PlayerRole::White => Some(GameResult::Wins(Players::Me)),
                PlayerRole::Black => Some(GameResult::Wins(Players::Other)),
            }
        }
    }
}

#[derive(Clone)]
pub struct BoardBlackGame;

impl BoardBlackGame {

    pub fn new() -> Self {
        BoardBlackGame
    }

}

impl Game<Board, Move> for BoardBlackGame {
    type Actions = BoardGameIter;

    fn apply(&self, state: &Board, action: &Move) -> Board {
        let mut state_clone: Board = (*state).clone();
        state_clone.apply_(*action);
        state_clone
    }

    fn my_actions(&self, state: &Board) -> Self::Actions {
        BoardGameIter(state.black_moves().collect())
    }

    fn other_actions(&self, state: &Board) -> Self::Actions {
        BoardGameIter(state.white_moves().collect())
    }

    fn terminal_state(&self, state: &Board) -> Option<GameResult> {
        let game_result = state.winner();

        if let None = game_result {
            return None;
        } else {
            match game_result.unwrap() {
                PlayerRole::Black => Some(GameResult::Wins(Players::Me)),
                PlayerRole::White => Some(GameResult::Wins(Players::Other)),
            }
        }
    }
}
