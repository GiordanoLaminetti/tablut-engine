#[cfg(test)]
use crate::games::tablut::Board;
use crate::games::tablut::Move;
use crate::games::tablut::TablutStateMessage;
use serde::{Deserialize, Serialize};

#[test]
fn test_valid() {
    let mut i = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;
    let mov = Move {
        from: (2, 3),
        to: (1, 3),
    };
    i.set([
        [e, e, e, b, b, b, e, e, e],
        [e, e, b, e, b, e, e, e, e],
        [e, w, e, w, e, e, e, e, e],
        [b, e, b, e, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    i.valid(&mov).unwrap();
}

#[test]
fn test_apply() {
    let mut i = Board::init();
    let mut c = Board::init();
    let e =  " ";
    let k = "KING";
    let b =  "BLACK";
    let w = "WHITE";
    let mov = Move {
        from: (3, 2),
        to: (3, 1),
    };
    i.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, w, e, e, w, e, e, e, e],
        [b, e, b, e, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    c.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, b, e, e, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    i.apply_(mov).unwrap();
    i.white.sort();
    i.black.sort();
    c.white.sort();
    c.black.sort();
    assert_eq!(i.white, c.white);     
    assert_eq!(i.black, c.black);     
    assert_eq!(i.king, c.king);
}

#[test]
#[should_panic]
fn test_not_valid_empty_move() {
    let mut i = Board::init();
     let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;
    let mov = Move {
        from: (3, 2),
        to: (3, 1),
    };
    i.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, w, e, e, w, e, e, e, e],
        [b, e, e, e, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    i.valid(&mov).unwrap();
}

#[test]
#[should_panic]
fn test_out_of_board_move() {
    let mut i = Board::init();
     let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;
    let mov = Move {
        from: (3, 8),
        to: (3, 9),
    };
    i.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, w, e, e, w, e, e, e, e],
        [b, e, e, e, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    i.valid(&mov).unwrap();
}

#[test]
#[should_panic]
fn test_not_empty_move() {
    let mut i = Board::init();
     let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;
    let mov = Move {
        from: (3, 2),
        to: (3, 1),
    };
    i.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, w, e, e, w, e, e, e, e],
        [b, b, b, e, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    i.valid(&mov).unwrap();
}

#[test]
#[should_panic]
fn test_move_black_camp() {
    let mut i = Board::init();
     let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;
    let mov = Move {
        from: (4, 2),
        to: (4, 1),
    };
    i.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, w, e, e, w, e, e, e, e],
        [b, b, b, e, w, e, e, e, b],
        [b, e, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    i.valid(&mov).unwrap();
}

#[test]
#[should_panic]
fn test_move_stazionary() {
    let mut i = Board::init();
     let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;
    let mov = Move {
        from: (4, 2),
        to: (4, 2),
    };
    i.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, w, e, e, w, e, e, e, e],
        [b, b, b, e, w, e, e, e, b],
        [b, e, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    i.valid(&mov).unwrap();
}

#[test]
#[should_panic]
fn test_move_castle() {
    let mut i = Board::init();
    let e =  " "  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;
    let mov = Move {
        from: (3, 4),
        to: (4, 4),
    };
    i.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, w, e, e, w, e, e, e, e],
        [b, b, b, e, w, e, e, e, b],
        [b, e, w, w, e, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    i.valid(&mov).unwrap();
}

#[test]
#[should_panic]
fn test_move_diagonally() {
    let mut i = Board::init();
     let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;
    let mov = Move {
        from: (4, 2),
        to: (3, 3),
    };
    i.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, w, e, e, w, e, e, e, e],
        [b, b, b, e, w, e, e, e, b],
        [b, e, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    i.valid(&mov).unwrap();
}

#[test]
#[should_panic]
fn test_move_across_other_pawn() {
    let mut i = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;
    let mov = Move {
        from: (3, 6),
        to: (3, 3),
    };
    i.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, w, e, e, w, e, e, e, e],
        [b, b, b, e, w, e, b, e, b],
        [b, e, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    i.valid(&mov).unwrap();
}

#[test]
#[should_panic]
fn test_move_across_castle() {
    let mut i = Board::init();
    let e =  " "  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;
    let mov = Move {
        from: (4, 6),
        to: (4, 3),
    };
    i.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, w, e, e, w, e, e, e, e],
        [b, b, b, e, w, e, b, e, b],
        [b, e, w, e, e, e, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    i.valid(&mov).unwrap();
}

#[test]
#[should_panic]
fn test_move_across_camp() {
    let mut i = Board::init();
    let e =  " "  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;
    let mov = Move {
        from: (3, 1),
        to: (5, 1),
    };
    i.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, w, e, e, w, e, e, e, e],
        [b, b, b, e, w, e, b, e, b],
        [b, e, w, e, e, e, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    i.valid(&mov).unwrap();
}

#[test]
fn test_not_move_across_camp() {
    let mut i = Board::init();
    let e =  " "  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;
    let mov = Move {
        from: (4, 8),
        to: (4, 6),
    };
    i.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, w, e, e, w, e, e, e, e],
        [b, b, b, e, w, e, b, e, b],
        [b, e, w, e, e, e, e, e, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    i.valid(&mov).unwrap();

}

#[test]
fn test_serialize_move() {
    let a = Move {
        to: (2, 3),
        from: (3, 2),
    };
    //let b = MoveMessage::init(a);
    let serialized = serde_json::to_string(&a).unwrap();
    assert_eq!("{\"to\":\"D3\",\"from\":\"C4\"}", serialized)
}

#[test]
fn test_deserialize_tablut() {
    let mut deserializzed : TablutStateMessage = serde_json::from_str("{\"board\":[[\"EMPTY\",\"EMPTY\",\"EMPTY\",\"BLACK\",\"BLACK\",\"BLACK\",\"EMPTY\",\"EMPTY\",\"EMPTY\"],[\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"BLACK\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\"],[\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"WHITE\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\"],[\"BLACK\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"WHITE\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"BLACK\"],[\"BLACK\",\"BLACK\",\"WHITE\",\"WHITE\",\"KING\",\"WHITE\",\"WHITE\",\"BLACK\",\"BLACK\"],[\"BLACK\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"WHITE\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"BLACK\"],[\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"WHITE\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\"],[\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"BLACK\",\"EMPTY\",\"EMPTY\",\"EMPTY\",\"EMPTY\"],[\"EMPTY\",\"EMPTY\",\"EMPTY\",\"BLACK\",\"BLACK\",\"BLACK\",\"EMPTY\",\"EMPTY\",\"EMPTY\"]],\"turn\":\"WHITE\"}").unwrap();
    let mut c = deserializzed.convert_state();
    let mut d = Board::init();
    c.white.sort();
    d.white.sort();
    c.black.sort();
    d.black.sort();
    assert_eq!(d.white, c.white);     
    assert_eq!(d.black, c.black);     
    assert_eq!(d.king, c.king);
}

/*
#[test]
fn test_csv_tablut(){
    let a = Board::init();
    let b = Board::init();
    let c: f64 = 10.1 ;
    assert_eq!("0,0,0,2,2,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,1,0,0,0,0,2,0,0,0,1,0,0,0,2,2,2,1,1,3,1,1,2,2,2,0,0,0,1,0,0,0,2,0,0,0,0,1,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,2,2,0,0,0,0,0,0,2,2,2,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,1,0,0,0,0,2,0,0,0,1,0,0,0,2,2,2,1,1,3,1,1,2,2,2,0,0,0,1,0,0,0,2,0,0,0,0,1,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,2,2,0,0,0,10.1\n",Board::to_csv(a,b,c))
}
*/