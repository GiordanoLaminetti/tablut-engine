#![allow(unused_imports)]
#[cfg(test)]
use crate::games::tablut::Board;
use crate::games::tablut::Move;

#[test]
fn test_black_eat() {
    let mut d = Board::init();
    let mut c = Board::init();
    let e =  " ";
    let k = "KING";
    let b =  "BLACK";
    let w = "WHITE";
    d.set([
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, w, e, e, w, e, e, e, e],
        [b, b, e, e, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    c.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, b, e, e, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    d.eat_(21).unwrap();
    assert_eq!(d.white, c.white);     
    assert_eq!(d.black, c.black);     
    assert_eq!(d.king, c.king);
}

#[test]
fn test_white_eat() {
    let mut d = Board::init();
    let mut c = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;
    let mov = Move {
        from: (3, 2),
        to: (3, 1),
    };

    d.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, w, e, e, b, e, e, e, e],
        [e, b, e, e, w, e, e, e, e],
        [b, w, e, e, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    c.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, w, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, w, e, e, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    d.eat_(21).unwrap();
    assert_eq!(d.white, c.white);     
    assert_eq!(d.black, c.black);     
    assert_eq!(d.king, c.king);
}

#[test]
fn test_black_border_eat() {
    let mut d = Board::init();
    let mut c = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    d.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [e, w, b, e, w, e, e, e, b],
        [e, e, w, w, k, w, w, b, b],
        [e, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    c.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [e, e, b, e, w, e, e, e, b],
        [e, e, w, w, k, w, w, b, b],
        [e, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    d.eat_(31).unwrap();
    assert_eq!(d.white, c.white);     
    assert_eq!(d.black, c.black);    
    assert_eq!(d.king, c.king);
}

#[test]
fn test_white_border_eat() {
    let mut d = Board::init();
    let mut c = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    d.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, b, w, e, w, e, e, e, b],
        [b, e, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    c.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, e, w, e, w, e, e, e, b],
        [b, e, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    d.eat_(31).unwrap();
    assert_eq!(d.white, c.white);    
    assert_eq!(d.black, c.black);     
    assert_eq!(d.king, c.king);
}

#[test]
fn test_white_king_anvil_eat() {
    let mut d = Board::init();
    let mut c = Board::init();
    let e = " ";
    let k = "KING";
    let b = "BLACK";
    let w = "WHITE";

    d.set([
        [e, e, e, b, b, b, e, e, e],
        [e, w, e, e, b, e, e, e, e],
        [e, b, e, e, w, e, e, e, e],
        [b, k, e, e, w, e, e, e, b],
        [b, b, w, w, e, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    c.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, w, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, k, e, e, w, e, e, e, b],
        [b, b, w, w, e, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    d.eat_(21).unwrap();
    d.white.sort();
    d.black.sort();
    c.white.sort();
    c.black.sort();
    assert_eq!(d.white, c.white);
    assert_eq!(d.black, c.black);
    assert_eq!(d.king, c.king);
}

#[test]
fn black_eat_king_in_castle() {
    let mut d = Board::init();
    let mut c = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    d.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, w, e, e, e, e, e, e, e],
        [b, b, e, e, b, e, e, e, b],
        [b, e, w, b, k, b, w, b, b],
        [b, e, e, e, b, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    c.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, w, e, e, e, e, e, e, e],
        [b, b, e, e, b, e, e, e, b],
        [b, e, w, b, e, b, w, b, b],
        [b, e, e, e, b, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    d.eat_(44).unwrap();
    assert_eq!(d.white, c.white);     
    assert_eq!(d.black, c.black);     
    assert_eq!(d.king, c.king);
}

#[test]
fn black_eat_king_adiacent_castle() {
    let mut d = Board::init();
    let mut c = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    d.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, e, e, e, e, e],
        [e, w, e, e, b, e, e, e, e],
        [b, b, e, b, k, b, e, e, b],
        [b, e, w, b, e, b, w, b, b],
        [b, e, e, e, b, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    c.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, e, e, e, e, e],
        [e, w, e, e, b, e, e, e, e],
        [b, b, e, b, e, b, e, e, b],
        [b, e, w, b, e, b, w, b, b],
        [b, e, e, e, b, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    d.eat_(34).unwrap();
    assert_eq!(d.white, c.white); 
    assert_eq!(d.black, c.black);     
    assert_eq!(d.king, c.king);
}

#[test]
fn black_eat_king_as_normal_pawn() {
    let mut d = Board::init();
    let mut c = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    d.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, k, e, e, w, e, e, e, e],
        [b, b, e, e, w, e, e, e, b],
        [b, b, w, w, e, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    c.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, b, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, b, e, e, w, e, e, e, b],
        [b, b, w, w, e, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    d.eat_(21).unwrap();
    assert_eq!(d.white, c.white);     
    assert_eq!(d.black, c.black);     
    assert_eq!(d.king, c.king);
}
#[test]
fn test_white_multiple_eat() {
    let mut d = Board::init();
    let mut c = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    d.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, w, e, e, b, e, e, e, e],
        [e, b, e, e, w, e, e, e, e],
        [e, e, w, w, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, w, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    c.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, w, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [e, w, e, w, w, e, e, e, b],
        [b, e, w, w, k, w, w, b, b],
        [b, w, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    d.apply_(Move{ from: (3, 2), to: (3, 1) }).unwrap();
    d.white.sort();
    d.black.sort();
    c.white.sort();
    c.black.sort();
    assert_eq!(d.white, c.white);     
    assert_eq!(d.black, c.black);     
    assert_eq!(d.king, c.king);
}
#[test]
fn test_o_side_eat() {
    let mut d = Board::init();
    let mut c = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    d.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, w, e, e, b, e, e, e, e],
        [e, b, e, e, w, e, e, e, e],
        [e, w, b, w, w, e, b, w, e],
        [b, b, w, w, k, w, w, b, b],
        [b, w, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    c.set( [
        [e, e, e, b, b, b, e, e, e],
        [e, w, e, e, b, e, e, e, e],
        [e, b, e, e, w, e, e, e, e],
        [e, w, b, w, w, e, b, e, e],
        [b, b, w, w, k, w, w, b, b],
        [b, w, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    d.eat_(37).unwrap();
    d.white.sort();
    d.black.sort();
    c.white.sort();
    c.black.sort();
    assert_eq!(d.white, c.white);    
    assert_eq!(d.black, c.black);     
    assert_eq!(d.king, c.king);
}

#[test]
fn test_n_side_eat() {
    let mut d = Board::init();
    let mut c = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    d.set( [
        [e, e, e, e, b, b, e, e, e],
        [e, w, e, w, b, e, e, e, e],
        [e, b, e, b, w, e, e, e, e],
        [e, w, b, w, w, e, b, w, e],
        [b, b, w, w, k, w, w, b, b],
        [b, w, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    c.set( [
        [e, e, e, e, b, b, e, e, e],
        [e, w, e, w, b, e, e, e, e],
        [e, e, e, b, w, e, e, e, e],
        [e, w, b, w, w, e, b, w, e],
        [b, b, w, w, k, w, w, b, b],
        [b, w, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    d.eat_(21).unwrap();
    d.white.sort();
    d.black.sort();
    c.white.sort();
    c.black.sort();
    assert_eq!(d.white, c.white);     
    assert_eq!(d.black, c.black);     
    assert_eq!(d.king, c.king);
}

#[test]
fn test_s_side_eat() {
    let mut d = Board::init();
    let mut c = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    d.set( [
        [e, e, e, e, b, b, e, e, e],
        [e, w, e, w, b, e, e, e, e],
        [e, b, e, b, w, e, e, e, e],
        [e, w, b, w, w, e, b, w, e],
        [b, b, w, w, k, w, w, b, b],
        [b, w, e, e, w, e, e, e, b],
        [e, e, e, b, w, e, e, e, e],
        [e, e, e, w, b, e, e, e, e],
        [e, e, e, e, b, b, e, e, e],
    ]);
    c.set( [
        [e, e, e, e, b, b, e, e, e],
        [e, w, e, w, b, e, e, e, e],
        [e, b, e, b, w, e, e, e, e],
        [e, w, b, w, w, e, b, w, e],
        [b, b, w, w, k, w, w, b, b],
        [b, w, e, e, w, e, e, e, b],
        [e, e, e, b, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, b, b, e, e, e],
    ]);
    d.eat_(73).unwrap();
    assert_eq!(d.white, c.white);     
    assert_eq!(d.black, c.black);     
    assert_eq!(d.king, c.king);
}

#[test]
fn test_eat_above_castle() {
    let mut d = Board::init();
    let mut c = Board::init();
    let e =  " "  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    d.set( [
        [e, e, e, e, b, b, e, e, e],
        [e, w, e, w, b, e, e, e, e],
        [e, b, e, b, b, e, e, e, e],
        [e, w, b, w, w, e, b, w, e],
        [b, b, w, w, e, w, w, b, b],
        [b, w, e, e, w, e, e, e, b],
        [e, e, e, b, w, e, e, e, e],
        [e, e, e, w, b, e, e, e, e],
        [e, e, e, e, b, b, e, e, e],
    ]);
    c.set( [
        [e, e, e, e, b, b, e, e, e],
        [e, w, e, w, b, e, e, e, e],
        [e, b, e, b, b, e, e, e, e],
        [e, w, b, w, e, e, b, w, e],
        [b, b, w, w, e, w, w, b, b],
        [b, w, e, e, w, e, e, e, b],
        [e, e, e, b, w, e, e, e, e],
        [e, e, e, w, b, e, e, e, e],
        [e, e, e, e, b, b, e, e, e],
    ]);
    d.eat_(34).unwrap();
    assert_eq!(d.white, c.white);     
    assert_eq!(d.black, c.black);     
    assert_eq!(d.king, c.king);
}
