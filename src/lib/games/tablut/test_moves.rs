#[cfg(test)]
use crate::games::tablut::{Board, Error, Move, Player};

#[test]
fn test_white_moves() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, w, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, e, b, e, w, e, e, e, b],
        [b, b, w, w, k, e, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);
    
    tablut.white_moves().for_each(|m| {
        assert_ne!(m.to.0, 9);
        assert_ne!(m.to.1, 9);
        assert_ne!(m.from.0, 9);
        assert_ne!(m.from.1, 9);
    });
    
}

#[test]
fn test_valid_out() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, w, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, e, b, e, w, e, e, e, b],
        [b, b, w, w, k, e, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    let m = Move {
        from: (1, 5),
        to: (1, 9),
    };
    let result = tablut.valid(&m);
    assert!(!tablut.valid(&m).is_ok());
    assert_eq!(result, Err(Error::MoveOutBoard(m)));
}

#[test]
fn test_black_moves() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, w, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, e, b, e, w, e, e, e, b],
        [b, b, w, w, k, e, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    tablut.black_moves().for_each(|m| {
        assert_ne!(m.to.0, 9);
        assert_ne!(m.to.1, 9);
        assert_ne!(m.from.0, 9);
        assert_ne!(m.from.1, 9);
    });
}

#[test]
fn test_winner_white() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, e, b, e, w, e, e, e, b],
        [b, b, w, w, e, e, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, k, e, e],
    ]);

    let winner = tablut.winner();
    assert_eq!(winner, Some(Player::White));
}

#[test]
fn test_winner_black() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, e, b, e, w, e, e, e, b],
        [b, b, w, w, e, e, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    let winner = tablut.winner();
    assert_eq!(winner, Some(Player::Black));
}

#[test]
fn test_no_winner() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, k, e, e],
        [b, e, b, e, w, e, e, e, b],
        [b, b, w, w, e, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    let winner = tablut.winner();
    assert_eq!(winner, None);
}

#[test]
fn num_white_moves() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, e, e, e, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    let num_moves: Vec<Move> = tablut.white_moves().collect();
    assert_eq!(num_moves.len(), 56);
}

#[test]
fn num_black_moves() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, e, e, e],
        [b, e, e, e, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    let num_moves: Vec<Move> = tablut.black_moves().collect();
    assert_eq!(num_moves.len(), 80);
}

#[test]
fn king_win_move() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, b, b, b, w, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, e, k, e, e],
        [b, e, e, e, w, e, e, e, b],
        [b, b, w, w, e, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    let win_move = tablut.king_win_move();
    assert_eq!(win_move, Some(Move{ from: (2, 6), to: (2, 8) }));
}

#[test]
fn can_eat_king() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, b, b, b, w, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, e, w, b, k, e, b],
        [b, e, e, e, w, e, e, e, b],
        [b, b, w, w, e, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    assert!(tablut.can_eat_king(Move{ from: (2,8), to: (2,7) }));
}

#[test]
fn can_eat_white() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [b, e, e, e, w, b, e, e, b],
        [b, e, e, e, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    assert!(tablut.can_eat(Move{ from: (2,0), to: (2,3) }));
}

#[test]
fn not_eat_white_on_move() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, e, b, b, e, e, e],
        [e, e, e, b, b, e, e, e, e],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, b, w, e, e, e, b],
        [b, b, w, w, k, w, w, b, b],
        [b, e, e, e, w, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    assert!(tablut.can_eat(Move{ from: (2, 4), to: (2, 3) }));
}

#[test]
fn not_eat_king_on_move() {
    let mut tablut = Board::init();
    let e =  " "  ;
    let k = "KING"  ;
    let b =  "BLACK"  ;
    let w = "WHITE"  ;

    tablut.set([
        [e, e, e, b, b, b, e, e, e],
        [e, e, e, e, e, e, k, e, e],
        [e, e, e, e, e, b, e, b, e],
        [b, e, e, e, e, e, e, e, e],
        [b, b, w, w, e, e, w, b, b],
        [b, e, e, e, e, e, e, e, b],
        [e, e, e, e, w, e, e, e, e],
        [e, e, e, e, b, e, e, e, e],
        [e, e, e, b, b, b, e, e, e],
    ]);

    assert_eq!(tablut.can_eat_king(Move{ from: (1,6), to: (2,6)}), false);
}
