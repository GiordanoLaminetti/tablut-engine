use std::rc::{Rc};
use crate::player::{Players, GameResult};
use itertools::Either;
use itertools::Either::{Left, Right};

/* UTILITY STRUCTURES */

/// Updates a path in the node tree with wins and played values, must be called on
/// the node from which it is called expand
pub struct NodeUpdate {
    pub owner_node: Players,
    indexes: Vec<usize>,
}

impl NodeUpdate {

    fn new() -> Self {
        NodeUpdate { indexes: vec![], owner_node: Players::Me }
    }

    fn set_owner(&mut self, owner: Players) {
        self.owner_node = owner;
    }

    fn append(&mut self, index: usize) {
        self.indexes.push(index)
    }
    
    pub fn pop(&mut self) {
        self.indexes.pop();
    }

    pub fn propagate<S, A>(&self, root: &mut Node<S, A>, wins: u32, played: u32) {
        let mut current_node = root;
        current_node.plays += played;
        if current_node.player == self.owner_node {
            current_node.wins += wins;
        } else {
            current_node.wins += played - wins;
        }

        for index in &self.indexes {
           current_node = &mut current_node.childrens[*index]; 
           current_node.plays += played;
           if current_node.player == self.owner_node {
               current_node.wins += wins;
           } else {
               current_node.wins += played - wins;
           }
        }
    }
    
    pub fn retrieve<'a, S, A>(&self, root: &'a mut Node<S, A>) -> &'a mut Node<S, A> {
        let mut current_node = root;

        for index in &self.indexes {
           current_node = &mut current_node.childrens[*index]; 
        }

        current_node
    }

}

/* NODE OF SEARCH TREE DEFINITION */

/// Expansion of a Node
#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Expansion {
    Full,
    Partial,
    Not,
}

/// If a state is terminal or not
#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Leaf {
    No,
    Defeat,
    Victory,
}

/// Defines a node of the search tree which contains info about the content status. Each node has
/// many childrens and owns them but also references its father without owning it.
pub struct Node<S, A> {

    // state components
    pub father_state: Rc<S>,
    pub state: Rc<S>,
    pub action: Option<A>,

    // stats
    pub depth: u64,
    pub player: Players, // owner of the state
    pub wins: u32,
    pub plays: u32,
    pub terminal: Leaf,

    // tree structure
    pub remained_childrens: Vec<Node<S, A>>,
    pub expansion: Expansion,
    pub childrens: Vec<Node<S, A>>
}

impl<S, A> Node<S, A> {

    /// Initializes a root node
    pub fn root(state: S, player: Players) -> Self {
        let state = Rc::new(state);
        Node {
            father_state: Rc::clone(&state),
            state: state,
            action: None,
            depth: 0,
            player: player,
            wins: 0,
            plays: 1,
            terminal: Leaf::No,
            remained_childrens: vec![],
            expansion: Expansion::Not,
            childrens: vec![],
        }
    }

    pub fn expand<T, P, E>(&mut self, policy: P, expansion: E, terminal: T) -> Either<(&Node<S, A>, NodeUpdate), NodeUpdate>
        where P: Fn(u32, &Vec<Node<S, A>>) -> usize,
              E: Fn(&Players, &S) -> Vec<(S, A)>,
              T: Fn(&S) -> Option<GameResult>,
    {
        let mut updater = NodeUpdate::new();
        let mut current_node = self;
        while current_node.expansion == Expansion::Full && current_node.terminal == Leaf::No {
            let index = policy(current_node.plays, &current_node.childrens);
            updater.append(index);
            current_node = &mut current_node.childrens[index];
        }

        // check is a leaf and returns without expand
        if current_node.terminal != Leaf::No {
            current_node.expansion = Expansion::Full;
            updater.set_owner(current_node.player);
            return Right(updater);
        }

        // expand leaf
        match current_node.expansion {

            // if node is a new one not expanded
            Expansion::Not => {
                let mut childrens: Vec<Node<S, A>> = expansion(&current_node.player, &current_node.state)
                                    .into_iter()
                                    .map(|(s, m)| {

                                        let terminal = match (current_node.player.next_player(), terminal(&s)) {
                                            (_, None) => Leaf::No,
                                            (Players::Me, Some(GameResult::Wins(Players::Me))) => Leaf::Victory,
                                            (Players::Other, Some(GameResult::Wins(Players::Other))) => Leaf::Victory,
                                            _ => Leaf::Defeat,
                                        };
                                        
                                        Node {
                                            father_state: Rc::clone(&current_node.state),
                                            state: Rc::new(s),
                                            action: Some(m),
                                            depth: current_node.depth + 1,
                                            player: current_node.player.next_player(),
                                            wins: 0,
                                            plays: 1,
                                            terminal: terminal,
                                            remained_childrens: vec![],
                                            expansion: Expansion::Not,
                                            childrens: vec![],
                                        }
                                    })
                                    .filter(|n| n.terminal != Leaf::Defeat )
                                    .collect();


                let choosen = childrens.pop();
                match choosen {
                    Some(n) => {
                        current_node.childrens.push(n);
                        current_node.remained_childrens = childrens;
                        current_node.expansion = Expansion::Partial;
                    },
                    None   => { 
                        current_node.expansion = Expansion::Full;
                        current_node.terminal = Leaf::Victory; // victory of the owner of the state cause other player cannot make moves
                        updater.set_owner(current_node.player);
                        return Right(updater);
                    }
                }
            },

            // if node is not full expanded but only partial
            Expansion::Partial => {
                let choosen = current_node.remained_childrens.pop();
                match choosen {
                    Some(n) => {
                            current_node.childrens.push(n);
                            if current_node.remained_childrens.is_empty() { current_node.expansion = Expansion::Full }
                    },
                    None    => current_node.expansion = Expansion::Full,
                }
            }

            Expansion::Full => { /* can't happen */ },
        }

        // here only for partial expanded nodes
        let size = current_node.childrens.len();
        updater.set_owner(current_node.player.next_player());
        updater.append(size - 1);
        Left((&current_node.childrens[size - 1], updater))
    }
    
    /// extracts a branch from current root taking ownership of it if
    /// it contains passed state, this functions searches it between childrens
    /// whose state owns to current player
    ///     R       <- Me
    /// /   |   \
    /// A   B   C   <- Other <<Between these>>
    pub fn extract_branch(&mut self, state: &S) -> Option<Node<S, A>>
        where S: PartialEq
    {
        let position = self.childrens.iter().position(|n| *n.state == *state)?;
        let node = self.childrens.remove(position);

        Some(node)
    }

}

/* TESTS */

#[cfg(test)]
mod tests {

    use std::rc::{Rc};

    use crate::player::{Players};
    use super::{Node, NodeUpdate, Expansion};

    #[test]
    fn root_node_creation() {
        let root: Node<u32, u32> = Node::root(0, Players::Me);

        assert_eq!(root.state, Rc::new(0));
        assert_eq!(root.player, Players::Me);
    }


    #[test]
    fn root_node_expansion() {

        // check initial state
        let mut root: Node<u32, u32> = Node::root(0, Players::Me);
        assert_eq!(root.expansion, Expansion::Not);
        assert_eq!(root.childrens.len(), 0);

        // expand and check first children
        {
            let (expanded, _) = root.expand(
                |vec| { unimplemented!() },          // policy, should be unused
                |p, s| { vec![(s+1, 0), (s+2, 1)] }, // expansion, returns all possible (state, action) for node to be expanded
            ).left().unwrap();

            assert_eq!(expanded.state, Rc::new(2));
            assert_eq!(expanded.action, Some(1));
        }

        // check root end state
        assert_eq!(root.childrens.len(), 1);
        assert_eq!(root.expansion, Expansion::Partial);

        // expand and check second children
        {
            let (expanded, _) = root.expand(
                |vec| { unimplemented!() },          // policy, should be unused
                |p, s| { vec![(s+1, 0), (s+2, 1)] }, // expansion, returns all possible (state, action) for node to be expanded
            ).left().unwrap();

            assert_eq!(expanded.state, Rc::new(1));
            assert_eq!(expanded.action, Some(0));
        }

        // check root end state
        assert_eq!(root.childrens.len(), 2);
        assert_eq!(root.expansion, Expansion::Full);

    }

    #[test]
    fn node_update() {
        let mut root: Node<u32, u32> = Node::root(0, Players::Me);

        // expand and backpropagate
        {
            let (_, update) = root.expand(
                |vec| { unimplemented!() },          // policy, should be unused
                |p, s| { vec![(s+1, 0), (s+2, 1)] },
            ).left().unwrap();
            update.propagate(&mut root, 5, 10);
        }
        
        assert_eq!(root.wins, 5);
        assert_eq!(root.plays, 11);

    }

    #[test]
    fn node_update_twice() {
        let mut root: Node<u32, u32> = Node::root(0, Players::Me);

        // expand twice and backpropagate
        {
            let (_, update1) = root.expand(
                |vec| { unimplemented!() },
                |p, s| { vec![(s+1, 0), (s+2, 1)] },
            ).left().unwrap();
            update1.propagate(&mut root, 5, 10);

            let (_, update2) = root.expand(
                |vec| { unimplemented!() },
                |p, s| { vec![(s+1, 0), (s+2, 1)] },
            ).left().unwrap();
            update2.propagate(&mut root, 1, 20);
        }

        assert_eq!(root.wins, 6);
        assert_eq!(root.plays, 31);

    }

    #[test]
    fn node_policy_usage() {
        let mut root: Node<u32, u32> = Node::root(0, Players::Me);

        // expand twice and backpropagate
        {
            let (_, update1) = root.expand(
                |vec| { unimplemented!() },
                |p, s| { vec![(s+1, 0), (s+2, 1)] },
            ).left().unwrap();
            update1.propagate(&mut root, 5, 10);

            let (_, update2) = root.expand(
                |vec| { unimplemented!() },
                |p, s| { vec![(s+1, 0), (s+2, 1)] },
            ).left().unwrap();
            update2.propagate(&mut root, 1, 20);
        }

        // check expanded node is first children
        {
            let (_, update) = root.expand(
                |vec| { 0 }, // choose first children
                |p, s| { vec![(s+1, 0), (s+2, 1)] },
            ).left().unwrap();
            let indexes = update.indexes;
            assert_eq!(indexes, vec![0, 0]);
        }

    }

    #[test]
    fn extract_branch_empty_root() {
        let mut root: Node<u32, u32> = Node::root(0, Players::Me);
        assert!(root.extract_branch(&1).is_none());
    }

    #[test]
    fn extract_branch_singleton() {
        let mut root: Node<u32, u32> = Node::root(0, Players::Me);
        assert!(root.extract_branch(&0).is_none());
    }

}
