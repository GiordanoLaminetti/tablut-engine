/* REPORT STRUCTURES */
use serde::{Deserialize, Serialize};

/// Simple structure for reporting stats on a play call
#[derive(Debug, PartialEq, Clone, Copy, Serialize, Deserialize)]
pub struct MonteCarloPlayReport<A> {
    // state and action choosen
    pub choosen_move: Option<A>,
    pub choosen_move_ratio: f64,

    // stats on choices
    pub num_childrens_expanded: usize,
    pub tot_childrens: usize,
    pub choice_ratio_var: f64,
    pub choice_ratio_avg: f64,
    pub choice_ratio_max: f64,
    pub choice_ratio_min: f64,

    // stats on tree topology
    pub max_depth_reached: u64,
    pub num_simulated_nodes: u64,
}

impl<A> MonteCarloPlayReport<A> {
    pub fn new() -> Self {
        MonteCarloPlayReport {
            choosen_move: None,
            choosen_move_ratio: 0.,
            num_childrens_expanded: 0,
            tot_childrens: 0,
            choice_ratio_var: 0.,
            choice_ratio_avg: 0.,
            choice_ratio_max: 0.,
            choice_ratio_min: 0.,
            max_depth_reached: 0,
            num_simulated_nodes: 0,
        }
    }
}
