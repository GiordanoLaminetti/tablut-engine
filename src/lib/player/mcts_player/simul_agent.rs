use std::thread;
use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver, SendError, RecvError};

use crate::player::{Players};
use crate::player::mcts_player::*;

/* Error */
#[derive(Debug)]
pub enum Error {
    PoolRecvError,
    PoolSendError,
}

impl From<RecvError> for Error {
    fn from(_: RecvError) -> Self {
        Error::PoolRecvError
    }
}

impl<T> From<SendError<T>> for Error {
    fn from(_: SendError<T>) -> Self {
        Error::PoolSendError
    }
}

/* STRUCTS */

enum Message<S> {
    Simulation(S, Players),
    UpdateNumSimulations(u32),
}

pub struct Reply {
    pub wins: u32,
    pub played: u32,
}

fn simulation_agent<G, S, A>(where_response: Sender<Reply>, 
                                limit_time: u64,
                                n_simulations: u32,
                                game: G, 
                                mut me_player: impl SimulationPlayer<G, S, A> + Send + 'static, 
                                mut other_player: impl SimulationPlayer<G, S, A> + Send + 'static) -> Sender<Message<S>>
    where G: Game<S, A> + Send + Clone + 'static,
          S: Send + 'static,
{
    let (sender, receiver): (Sender<Message<S>>, Receiver<Message<S>>) = mpsc::channel();
    
    thread::spawn(move || {

        let mut n_simulations = n_simulations;
        let mut game = game;
        // process messages until channel is closed
        for message in receiver {
            match message {
                Message::UpdateNumSimulations(new_simulations) => {
                    n_simulations = new_simulations;
                },
                Message::Simulation(state, player) => {
                    
                    // time watcher
                    let start_time = SystemTime::now();

                    let limit_time_val = Duration::new(limit_time, 0);
                    let time_end = || {
                        match start_time.elapsed() {
                            Err(_) => true,
                            Ok(n) if n >= limit_time_val => true,
                            Ok(_) => false,
                        }
                    };

                    // start simulations
                    let mut wins = 0;
                    let mut played = 0;

                    let me_simul_game = SimulationGame { switch: false, game: &mut game.clone() };
                    let other_simul_game = SimulationGame { switch: true, game: &mut game.clone() };

                    let rc_state = Rc::new(state);
                    for _ in 0.. n_simulations {

                        // check time
                        if time_end() { break }

                        // compute
                        let mut current_state = Rc::clone(&rc_state);
                        let mut current_owner = player;

                        loop {

                            // check time
                            if time_end() { break }

                            match game.terminal_state(&current_state) {
                                None => { /* DO NOTHING */ }
                                Some(GameResult::Stalemate) => {
                                    played += 1;
                                    break;
                                }
                                Some(GameResult::Wins(Players::Me)) => {
                                    wins += 1;
                                    played += 1;
                                    break;
                                }
                                Some(GameResult::Wins(Players::Other)) => {
                                    played += 1;
                                    break;
                                }
                            };

                            let action : Option<A> = match current_owner {
                                Players::Other => me_player.next_move(&me_simul_game, &current_state),
                                Players::Me => other_player.next_move(&other_simul_game, &current_state),
                            };

                            if let None = action {
                                if let Players::Me = current_owner { wins += 1 }
                                played += 1;
                                break;
                            }

                            current_state = Rc::new(game.apply(&current_state, &action.unwrap()));
                            current_owner = current_owner.next_player();
                        }
                    }

                    where_response.send(Reply{ wins, played }).unwrap(); // TODO check Result
                },
            }
        }
    });

    sender
}

/* POOL STRUCT */

pub struct SimulationPool<S> {
    receiver: Receiver<Reply>,
    agents_ch: Vec<Sender<Message<S>>>,
}

impl<S> SimulationPool<S> {

    pub fn new<G, A>(agent_num: usize,
               limit_time: u64, 
               n_simulations: u32,
               game: G, 
               me_player: impl SimulationPlayer<G, S, A> + Send + 'static + Clone,
               other_player: impl SimulationPlayer<G, S, A> + Send + 'static + Clone) -> Self
        where G: Game<S, A> + Send + 'static + Clone,
              S: Send + 'static,
    {

        let mut agents_ch = Vec::new();
        let (where_response, receiver) = mpsc::channel();
        for _ in 0 .. agent_num {
            let where_write = simulation_agent(where_response.clone(), limit_time, n_simulations, game.clone(), me_player.clone(), other_player.clone());
            agents_ch.push(where_write);
        }

        SimulationPool { receiver, agents_ch }
    }

    pub fn broadcast(&self, state: &S, player: Players) -> Result<(), Error>
        where S: Clone,
    {
        for sender in &self.agents_ch {
            sender.send(Message::Simulation(state.clone(), player))?;
        }

        Ok(())
    }

    pub fn recv(&self) -> Result<Vec<Reply>, Error> {
        let mut responses = Vec::new();

        for _ in 0 .. self.agents_ch.len() {
            responses.push(self.receiver.recv()?);
        }
        
        Ok(responses)
    }

    pub fn set_n_simulations(&self, n_simulations: u32) -> Result<(), Error> {
        for sender in &self.agents_ch {
            sender.send(Message::UpdateNumSimulations(n_simulations))?;
        }

        Ok(())
    }

}




