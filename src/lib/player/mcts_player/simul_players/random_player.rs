use crate::player::mcts_player::{SimulationPlayer, SimulationGame};
use crate::player::{Game};
use rand;
use rand::Rng;

/// Implementation of a Random Player
/// which chooses a random action from
/// the available ones
#[derive(Clone, Copy)]
pub struct RandomPlayer;

impl RandomPlayer
{
    pub fn new() -> Self
    {
        RandomPlayer
    }
}

// IMPLS //

impl<G, S, A> SimulationPlayer<G, S, A> for RandomPlayer
    where G: Game<S, A>,
{
    fn next_move(&mut self, game: &SimulationGame<G>, state: &S) -> Option<A> {
        let mut rand_gen = rand::thread_rng();
        let mut actions : Vec<A> = game.my_actions(&state).collect();

        match actions.len() {
            0 => None,
            len => {
                let num: usize = rand_gen.gen_range(0, len);
                let choice = actions.remove(num);
                Some(choice)
            }
        }
    }
}
