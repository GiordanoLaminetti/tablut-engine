use crate::player::Player;
use rand;
use rand::Rng;

/// Implementation of a Random Player
/// which chooses a random action from
/// the available ones
pub struct RandomPlayer<'a, S, A> {
    state_gen: Box<dyn Fn(S) -> Vec<A> + 'a>,
    rand_gen: rand::rngs::ThreadRng,
}

impl<'a, S, A> RandomPlayer<'a, S, A> {
    pub fn new<F: Fn(S) -> Vec<A> + 'a>(f: F) -> Self {
        RandomPlayer {
            state_gen: Box::new(f),
            rand_gen: rand::thread_rng(),
        }
    }
}

impl<'a, S, A> Player<S, A> for RandomPlayer<'a, S, A> {
    fn next_move(&mut self, state: S) -> Option<A> {
        let mut states = (self.state_gen)(state);

        match states.len() {
            0 => None,
            len => {
                let num: usize = (self.rand_gen).gen_range(0, len);
                let choice = states.remove(num);
                Some(choice)
            }
        }
    }
}

// TESTS //

#[cfg(test)]
mod tests {

    use crate::environment::tablut_competition_env::TablutCompetionEnv as TEnv;
    use crate::environment::GameEnv;
    use crate::games::tablut;
    use crate::player::random_player::RandomPlayer;
    use crate::player::Player;
    use std::iter;

    #[test]
    fn choose_correct_action() {
        let generator = |s| vec![s + 1, s - 1];
        let mut player = RandomPlayer::new(generator);
        let action = player.next_move(5);
        match action {
            Some(4) => (),
            Some(6) => (),
            None => panic!("Choosen nothing between 4, 6 values"),
            Some(x) => panic!("Choosen {} between 4, 6 values", x),
        }
    }

    #[test]
    fn test_probabilities() {
        let generator = |s| vec![s + 1, s - 1];
        let mut player = RandomPlayer::new(generator);

        let state = 5;

        let (equal4, equal6): (Vec<i32>, Vec<i32>) = iter::repeat(state)
            .map(|v| player.next_move(v).unwrap())
            .take(100)
            .partition(|&v| v == 4);

        let num4 = equal4.len() as i32;
        let num6 = equal6.len() as i32;
        let difference = i32::abs(num4 - num6);
        assert!(difference < 25);
    }

}
