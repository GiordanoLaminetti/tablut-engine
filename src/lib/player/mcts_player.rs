/// Implements a player which uses Monte Carlo Tree Search for a Zero-Sum 2 players game
mod node;
pub mod report;
pub mod simul_agent;
pub mod simul_players;

use crate::player::mcts_player::node::{Node, NodeUpdate, Leaf, Expansion};
use crate::player::mcts_player::report::MonteCarloPlayReport;
use crate::player::mcts_player::simul_agent::SimulationPool;
use crate::player::{Game, GameResult, Player, Players, Reporter};
use itertools::{Either};
use itertools::Either::{Left, Right};
use num_cpus;
use serde::Serialize;
use std::f64;
use std::rc::Rc;
use std::time::{Duration, SystemTime};

/* ERRORS */

#[derive(Debug)]
pub enum Error {
    PoolError(simul_agent::Error),
    ActionNotFound,
    PoolNotUsed,
}

impl From<simul_agent::Error> for Error {
    fn from(error: simul_agent::Error) -> Error {
        Error::PoolError(error)
    }
}

/* PLAYERS FOR MONTECARLO PLAYER */

pub struct SimulationGame<'a, G> {
    switch: bool,
    game: &'a mut G,
}

impl<'a, G> SimulationGame<'a, G> {
    pub fn apply<S, A>(&self, state: &S, action: &A) -> S
    where
        G: Game<S, A>,
    {
        self.game.apply(state, action)
    }

    pub fn my_actions<S, A>(&self, state: &S) -> impl Iterator<Item = A>
    where
        G: Game<S, A>,
    {
        match self.switch {
            false => self.game.my_actions(state),
            true => self.game.other_actions(state),
        }
    }

    pub fn other_actions<S, A>(&self, state: &S) -> impl Iterator<Item = A>
    where
        G: Game<S, A>,
    {
        match self.switch {
            false => self.game.other_actions(state),
            true => self.game.my_actions(state),
        }
    }

    pub fn terminal_state<S, A>(&mut self, state: &S) -> Option<GameResult>
    where
        G: Game<S, A>,
    {
        let switch_result = |game_res| match game_res {
            GameResult::Wins(Players::Me) => GameResult::Wins(Players::Other),
            GameResult::Wins(Players::Other) => GameResult::Wins(Players::Me),
            stalemate => stalemate,
        };

        match self.switch {
            false => self.game.terminal_state(state),
            true => {
                let res = self.game.terminal_state(state)?;
                Some(switch_result(res))
            }
        }
    }
}

pub trait SimulationPlayer<G, S, A>
where
    G: Game<S, A>,
{
    fn next_move(&mut self, game: &SimulationGame<G>, state: &S) -> Option<A>;
}

/* FUNCTIONS */

// panics if 'nodes' is empty !!
pub type Wins = u32;
pub type Played = u32;

fn ucb1_internal(r: f64, tot_played: Played, from_choose: &Vec<(Wins, Played)> ) -> usize {
    let num_plays = tot_played as f64;

    let mut node1 = &from_choose[0];
    let mut choosen = 0;

    for curr_index in 0 .. from_choose.len() {
        let node2 = &from_choose[curr_index];

        let mean_v1: f64 = node1.0 as f64 / node1.1 as f64;
        let mean_v2: f64 = node2.0 as f64 / node2.1 as f64;

        let bound_v1 = mean_v1 + r * f64::sqrt(num_plays.ln() / (node1.1 as f64));
        let bound_v2 = mean_v2 + r * f64::sqrt(num_plays.ln() / (node2.1 as f64));

        if bound_v2 > bound_v1 {
            node1 = node2;
            choosen = curr_index;
        }
    }

    choosen
}

pub fn ucb1(r: f64) ->  impl Fn(Played, &Vec<(Wins, Played)>) -> usize
{
    move |p, v| ucb1_internal(r, p, v)
}

/* MONTE CARLO PLAYER */

/// Defines a MonteCarloPlayer which contains a game and a
/// limit of time for simulations
pub struct MonteCarloPlayer<G, S, A> {
    // settings
    limit_time: Duration,

    // game
    game: G,
    cache: Box<Node<S, A>>,

    // simulations
    evaluator: Either<SimulationPool<S>, (Box<Fn(&S) -> (u32, u32)>, Box<Fn(&S) -> (u32, u32)>)>,
    policy: Rc<Fn(Played, &Vec<(Wins, Played)>) -> usize>,

    // reports
    reports_enabled: bool,
    current_report: MonteCarloPlayReport<A>,
    reports: Vec<MonteCarloPlayReport<A>>,
}

impl<G, S, A> MonteCarloPlayer<G, S, A> {

    /* COSTRUCTOR and SETTINGS */

    /// builds a new MonteCarloPlayer given a time_limit and a game
    /// instance
    pub fn new<P, Q>(
        reports: bool,
        game: G,
        init_state: S,
        me_player: P,
        other_player: Q,
        limit_time: u64,
        n_simulations: u32,
    ) -> Self
    where
        G: Game<S, A> + Clone + Send + 'static,
        P: SimulationPlayer<G, S, A> + Clone + Send + 'static,
        Q: SimulationPlayer<G, S, A> + Clone + Send + 'static,
        S: Send + 'static,
    {
        let cache = Box::new(Node::root(init_state, Players::Other));

        // compute number of simulations for single cpu
        let n_simulations = if n_simulations == 0 { 1 } else { n_simulations };
        let mut number_of_cpus = num_cpus::get();
        if number_of_cpus > n_simulations as usize {
            number_of_cpus = n_simulations as usize;
        }
        let simuls_for_cpu = n_simulations / number_of_cpus as u32;

        // init a pool of agents
        let evaluator = Left(SimulationPool::new(
            num_cpus::get() as usize,
            limit_time,
            simuls_for_cpu,
            game.clone(),
            me_player,
            other_player,
        ));
        let limit_time = Duration::new(limit_time, 0);

        MonteCarloPlayer {
            limit_time,
            game,
            cache,
            evaluator,
            policy: Rc::new(ucb1(1.41)),
            reports_enabled: reports,
            current_report: MonteCarloPlayReport::new(),
            reports: Vec::new(),
        }
    }

    pub fn new_with_heuristic<P, Q>(
        reports: bool,
        game: G,
        init_state: S,
        heuristic_me: P,
        heuristic_other: Q,
        limit_time: u64,
    ) -> Self
    where 
        P: Fn(&S) -> (u32, u32) + 'static,
        Q: Fn(&S) -> (u32, u32) + 'static,
    {
        MonteCarloPlayer {
            limit_time: Duration::new(limit_time, 0),
            game,
            cache: Box::new(Node::root(init_state, Players::Other)),
            evaluator: Right((Box::new(heuristic_me), Box::new(heuristic_other))),
            policy: Rc::new(ucb1(1.41)),
            reports_enabled: reports,
            current_report: MonteCarloPlayReport::new(),
            reports: Vec::new(),
        }
    }

    /// sets policy used, default is ucb1(1.)
    pub fn set_policy<P>(&mut self, policy: P)
        where P: Fn(Played, &Vec<(Wins, Played)>) -> usize + 'static
    {
        self.policy = Rc::new(policy);
    }

    /// sets number of simulations to run in each thread for each simulated node
    pub fn set_n_simulations(&self, n: u32) -> Result<(), Error> {
        match &self.evaluator {
            Left(sim_pool) => { sim_pool.set_n_simulations(n)?; },
            Right(_) => { return Err(Error::PoolNotUsed); },
        };

        Ok(())
    }

    /* REPORT METHODS */

    fn set_in_report(&mut self, name: &str, value: f64) {
        match name {
            "choosen_move_ratio" => self.current_report.choosen_move_ratio = value,
            "tot_childrens" => self.current_report.tot_childrens = value as usize,
            "choice_ratio_var" => self.current_report.choice_ratio_var = value,
            "choice_ratio_avg" => self.current_report.choice_ratio_avg = value,
            "choice_ratio_max" => self.current_report.choice_ratio_max = value,
            "choice_ratio_min" => self.current_report.choice_ratio_min = value,
            "max_depth_reached" => self.current_report.max_depth_reached = value as u64,
            "num_simulated_nodes" => self.current_report.num_simulated_nodes = value as u64,
            "num_childrens_expanded" => self.current_report.num_childrens_expanded = value as usize,
            _ => { panic!("invalid {}", name); },
        }
    }

    fn set_in_report_action(&mut self, action: A)
        where A: Clone
    {
        self.current_report.choosen_move = Some(action.clone())
    }

    pub fn clear_current_report(&mut self) {
        self.current_report = MonteCarloPlayReport::new();
    }

    fn commit_report(&mut self)
        where A: Clone,
    {
        self.reports.push(self.current_report.clone());
        self.clear_current_report();
    }

    /* METHODS */

    /// returns best action computed with MCTS
    pub fn play(&mut self, state: S) -> Result<A, Error>
        where G: Game<S, A> + Clone,
              S: PartialEq + Clone,
              A: Clone,
    {
        let actions_ratios = self.all_ratios(state)?;
        self.get_best(actions_ratios)
    }

    /// choose best option between nodes
    pub fn get_best(&mut self, ratios: Vec<(A, Leaf, u32, u32)>) -> Result<A, Error>
    where
        A: Clone,
    {
        // check no actions
        if ratios.is_empty() {
            if self.reports_enabled {
                let mut play_report = MonteCarloPlayReport::new();
                play_report.choosen_move = None;
                self.reports.push(play_report);
            }
            return Err(Error::ActionNotFound);
        }

        // check can win
        let ratios: Vec<(A, Leaf, u32, u32)> = ratios.into_iter().filter(|(_, t, _, _)| *t != Leaf::Defeat).collect();
        let win_move = ratios.iter().find(|(_, t, _, _)| *t == Leaf::Victory);
        if let Some(winner_state) = win_move {
            return Ok(winner_state.0.clone());
        }

        let ratios: Vec<(A, f64)> = ratios.into_iter().map(|(a, _, w, p)| (a, w as f64 / p as f64)).collect();

        // find best action
        let mut best_index = 0;
        let mut index = 0;
        let (mut best_action, mut best_ratio) = ratios[0].clone();
        for (a, r) in &ratios {
            if *r > best_ratio {
                best_ratio = *r;
                best_action = a.clone();
                best_index = index;
            }
            index += 1;
        }

        // update and return result
        let choosen = self.cache.childrens.remove(best_index);
        self.cache = Box::new(choosen);

        // reports
        if self.reports_enabled {
            self.set_in_report_action(best_action.clone());

            let mut max = f64::MIN;
            let mut min = f64::MAX;
            let mut sum = 0.;
            for (_, ratio) in &ratios {
                sum += ratio;
                if max < *ratio {
                    max = *ratio
                }
                if min > *ratio {
                    min = *ratio
                }
            }

            self.set_in_report("choosen_move_ratio", best_ratio);
            self.set_in_report("num_childrens_expanded", ratios.len() as f64);
            self.set_in_report("tot_childrens", (self.cache.childrens.len() + self.cache.remained_childrens.len() + 1) as f64);
            self.set_in_report("choice_ratio_avg", sum / ratios.len() as f64);
            self.set_in_report("choice_ratio_max", max);
            self.set_in_report("choice_ratio_min", min);

            sum = 0.;
            for (_, ratio) in &ratios {
                sum += (ratio - self.current_report.choice_ratio_avg) * (ratio - self.current_report.choice_ratio_avg);
            }

            self.current_report.choice_ratio_var = sum / ratios.len() as f64;
            self.commit_report();
        }

        Ok(best_action)
    }

    /// 4 steps of Monte Carlo Tree search:
    ///
    /// 1. selection
    /// 2. expansion
    /// 3. simulation
    /// 4. backpropagation
    ///
    /// state is the state from which the player will play.
    /// computes wins ratios for all possible actions from current state
    pub fn all_ratios(&mut self, state: S) -> Result<Vec<(A, Leaf, u32, u32)>, Error>
    where
        G: Game<S, A> + Clone,
        S: PartialEq + Clone,
        A: Clone,
    {
        // time watcher
        let start_time = SystemTime::now();
        let limit_time = self.limit_time.clone();
        let time_end = || match start_time.elapsed() {
            Err(_) => true,
            Ok(n) if n >= limit_time => true,
            Ok(_) => false,
        };

        // init or reuse states
        match self.cache.extract_branch(&state) {
            None => self.cache = Box::new(Node::root(state, Players::Other)),
            Some(node) => {
                self.cache = Box::new(node);
            }
        }

        // starts simulations
        let cache_depth = self.cache.depth;
        let mut max_depth = 0;
        let mut simulated_nodes = 0;
        loop {
            match time_end() {

                /* COMPUTE RESULT */
                true => { 

                    // check no childrens
                    if self.cache.childrens.is_empty() {
                        if self.reports_enabled {
                            let mut play_report = MonteCarloPlayReport::new();
                            play_report.choosen_move = None;
                            self.reports.push(play_report);
                        }

                        return Err(Error::ActionNotFound);
                    }

                    // reports
                    if self.reports_enabled {
                        self.set_in_report("num_simulated_nodes", simulated_nodes as f64);
                        self.set_in_report("max_depth_reached", (max_depth - self.cache.depth) as f64);
                    }

                    // obtain simulated ratios
                    let childrens_ratio: Vec<(A, Leaf, u32, u32)> = self.cache.childrens.iter()
                        .map(|node| (node.action.clone().unwrap(), node.terminal, node.wins, node.plays))
                        .collect();

                    return Ok(childrens_ratio)
                },

                /* IN TIME: continue simulations */
                false => {

                    // select and expand
                    let game = &mut self.game;
                    let game_to_move = game.clone();
                    let policy = Rc::clone(&self.policy);

                    let result = self.cache.expand(
                        |tot, vc| policy(tot, &vc.iter().map(|n| (n.wins, n.plays)).collect()),
                        |p, s| match p {
                            Players::Other => game
                                .my_actions(s)
                                .map(|a| (game.apply(s, &a), a))
                                .collect(),
                            Players::Me    => game
                                .other_actions(s)
                                .map(|a| (game.apply(s, &a), a))
                                .collect(),
                        },
                       move |s| { game_to_move.terminal_state(s) },
                    );

                    let current_node: &Node<S, A>;
                    let mut updater: NodeUpdate;
                    match result {
                        Left((c, u)) => {
                            current_node = c;
                            updater = u;
                        }
                        Right(u) => {
                            current_node = u.retrieve(&mut self.cache);
                            updater = u;
                        },
                    }

                    simulated_nodes += 1;
                    if current_node.depth > max_depth {
                        max_depth = current_node.depth;
                    }

                    let current_node_terminal = current_node.terminal;
                    let current_node_depth = current_node.depth;
                    let current_state = Rc::clone(&current_node.state);
                    if current_node_terminal != Leaf::No && current_node_depth != cache_depth + 1 {

                        println!("found terminal at depth {}", current_node_depth);

                        // if node is terminal with a Victory, its father is
                        // a Defeat so update father
                        if current_node.terminal == Leaf::Victory {
                            updater.pop();
                            let mut father = updater.retrieve(&mut self.cache);
                            father.terminal = Leaf::Defeat;
                            father.expansion = Expansion::Full;
                            father.childrens = vec![];
                            father.remained_childrens = vec![];
                        }
                        
                        // if node is a defeat its father will never choose it
                        // so it can be dropped
                        if current_node_terminal == Leaf::Defeat {
                            updater.pop();
                            let father = updater.retrieve(&mut self.cache);
                            father.extract_branch(&current_state);
                            if father.childrens.is_empty() {
                                father.terminal  = Leaf::Victory;
                                father.expansion = Expansion::Full;
                            }
                        }

                    } else {

                        match &self.evaluator {
                            Left(sim_pool) => {
                                let current_player = current_node.player;
                                sim_pool.broadcast(&current_node.state, current_node.player)?;
                                for reply in sim_pool.recv()? {
                                    match current_player {
                                        Players::Me => updater.propagate(&mut self.cache, reply.wins, reply.played),
                                        Players::Other => updater.propagate(&mut self.cache, reply.played - reply.wins, reply.played),
                                    }
                                }
                            },
                            Right((h_me, _)) => {
                                let (wins, played) = match current_node.player {
                                    Players::Me    => h_me(&current_node.state),
                                    Players::Other => { 
                                        let (w, p) = h_me(&current_node.state);
                                        (p - w, p)
                                    },
                                };
                                updater.propagate(&mut self.cache, wins, played);
                            },
                        }
                    } 
                    // end prediction or simulations

                }
            }
        }
    }


}

// IMPLS //

/* REPORTS GETTER */

impl<G, S, A> Reporter<Vec<MonteCarloPlayReport<A>>> for MonteCarloPlayer<G, S, A>
where
    A: Serialize,
{
    /// return reports collected and clean player reports memory
    fn report(&mut self) -> Option<Vec<MonteCarloPlayReport<A>>> {
        if self.reports_enabled {
            return Some(self.reports.drain(..).collect());
        }
        None
    }
}

impl<G, S, A> Player<S, A> for MonteCarloPlayer<G, S, A>
where
    A: Clone,
    S: PartialEq + Clone,
    G: Game<S, A> + Clone,
{
    fn next_move(&mut self, state: S) -> Option<A> {
        if let Ok(res) = self.play(state) {
            return Some(res);
        }
        None
    }
}

// TESTS //

#[cfg(test)]
mod tests {

    use super::*;
    use crate::player::mcts_player::simul_players::random_player::RandomPlayer;
    use crate::player::*;
    use std::slice::Iter;

    // FAKE GAME //
    // it is a little simple game in which a player must step over
    // 20 with -1, 0, +1, +5 and opponent player must try to reach
    // a 5 multiple.

    type State = i64;
    type Move = i64;

    struct MoveIter {
        moves: Vec<Move>,
    }

    impl Iterator for MoveIter {
        type Item = Move;

        fn next(&mut self) -> Option<Move> {
            self.moves.pop()
        }
    }

    #[derive(Clone, Copy)]
    struct FakeGame;

    impl Game<State, Move> for FakeGame {
        type Actions = MoveIter;

        fn apply(&self, state: &State, action: &Move) -> State {
            state + action
        }

        fn my_actions(&self, _: &State) -> MoveIter {
            let moves = vec![-1, 1, 0, 10];
            MoveIter { moves }
        }

        fn other_actions(&self, _: &State) -> MoveIter {
            let moves = vec![-4, -6, 4, 6];
            MoveIter { moves }
        }

        fn terminal_state(&mut self, state: &State) -> Option<GameResult> {
            match *state {
                x if x % 5 == 0 => Some(GameResult::Wins(Players::Other)),
                x if x >= 20 => Some(GameResult::Wins(Players::Other)),
                x if x <= 1000 => Some(GameResult::Stalemate),
                _ => None,
            }
        }
    }

    // TESTS //

    #[test]
    fn montecarlo_player_new() {
        let game = FakeGame;
        let mut player = MonteCarloPlayer::new(
            false,
            game,
            0,
            RandomPlayer::new(),
            RandomPlayer::new(),
            2,
            10,
        );
    }

    #[test]
    fn montecarlo_player_compute_action() {
        let game = FakeGame;
        let mut player = MonteCarloPlayer::new(
            false,
            game,
            0,
            RandomPlayer::new(),
            RandomPlayer::new(),
            2,
            10,
        );

        let next_move = player.play(0);
    }

}
