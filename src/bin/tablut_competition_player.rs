extern crate tablut_engine;

use getopts::Options;
use serde::{Deserialize, Serialize};
use std::env;
use std::fs;
use std::io;
use std::num::ParseIntError;
use std::process::exit;
use std::time::SystemTime;
use tablut_engine::environment::tablut_competition_env::TablutCompetionEnv as TEnv;
use tablut_engine::environment::GameEnv;
use tablut_engine::games::tablut::game_board::*;
use tablut_engine::games::tablut::players::cnn_player::CnnPlayer;
use tablut_engine::games::tablut::Player as PlayerRole;
use tablut_engine::games::tablut::{Board, Move, TablutStateMessage};
use tablut_engine::player::mcts_player::{ucb1, MonteCarloPlayer};
use tablut_engine::player::{GameResult, Player, Players, Reporter};

//[> MAIN ERROR <]
enum Error {
    ResponseError(String),
    ReplyError(String),
    SendNameError(String),
    ReportError(String),
    ParsingError(String),
    UnknownResponseError,
}

impl From<ParseIntError> for Error {
    fn from(e: ParseIntError) -> Error {
        Error::ParsingError(format!("{}", e))
    }
}

//[> STRUCTS <]

/// Configuration struct for the tablut player binary
#[derive(Deserialize, Serialize, Debug)]
struct Configuration {
    server_address: String,
    white_port: String,
    black_port: String,
    team_name: String,
    time_limit: u64,
    white_cnn_path: String,
    black_cnn_path: String,
    white_exploration_ratio: Option<f64>,
    black_exploration_ratio: Option<f64>,
}

#[derive(Serialize)]
struct ReportWrapper<R>
where
    R: Serialize,
{
    configuration: Configuration,
    report: R,
    game_result: Option<GameResult>,
}

//[> AUXILIARY FUNCTIONS <]

/// Reads from the file into a Configuration structure
fn read_conf(file: &str) -> Result<Configuration, io::Error> {
    let file_content = fs::read_to_string(file)?;
    let conf = serde_json::from_str(&file_content)?;
    Ok(conf)
}

/// Reads the command line argument for the role and returns an
/// adequate PlayerRole
fn parse_role(arg: &str) -> Result<PlayerRole, String> {
    match arg {
        "white" => Ok(PlayerRole::White),
        "black" => Ok(PlayerRole::Black),
        _ => Err(String::from("")),
    }
}

/// Prints the help to stdout
fn print_usage(program: &str, opts: Options) {
    let usage = format!("Usage: {} <white|black> [options]", program);
    print!("{}", opts.usage(&usage));
}

/// Prints the correct message on stderr given an error
fn print_error(error: Error) {
    match error {
        Error::SendNameError(msg) => eprintln!("Main: couldn't send name: {}", msg),
        Error::ResponseError(msg) => eprintln!("Main: response error: {}", msg),
        Error::ReplyError(msg) => eprintln!("Main: reply error: {}", msg),
        Error::ReportError(msg) => eprintln!("Main: couldn't save report: {}", msg),
        Error::UnknownResponseError => eprintln!("Main: unknown response form server"),
        Error::ParsingError(msg) => eprintln!("Main: parsing error: {}", msg),
    }
}

fn print_egg() {
    println!(
        "Tafl emk örr at efla ♖ 
íþróttir kannk níu  
týnik trauðla rúnum 
tíð er mér bók ok smíðir 📚 

Skríða kannk á skíðum ⛷️
skýtk ok ræ´k, svát nýtir 🏹
hvárt tveggja kannk hyggja 🛶
harpslótt ok bragþóttu 🎸\n"
    );
}

/// Prints the correct message on stdout given a game result
fn print_result(result: &GameResult) {
    match result {
        GameResult::Wins(Players::Me) => println!("Main: you win! 🍻🥂🎉🍾"),
        GameResult::Wins(Players::Other) => println!("Main: you lost! ☠️ "),
        GameResult::Stalemate => println!("Main: game ends with a draw!"),
    }
}

/// Saves the report to the reports_file in yaml format
fn report<R>(
    reports_file: &str,
    report: R,
    configuration: Configuration,
    game_result: Option<GameResult>,
) -> Result<(), Error>
where
    R: Serialize,
{
    let report_wrapper = ReportWrapper {
        report,
        configuration,
        game_result,
    };

    match fs::write(
        reports_file,
        serde_yaml::to_string(&report_wrapper).unwrap(),
    ) {
        Ok(_) => Ok(()),
        Err(e) => Err(Error::ReportError(format!("{}", e))),
    }
}

/// App logic for the Tablut Competition 2018 - 2019
fn app_logic(
    player: &mut Player<Board, Move>,
    player_role: PlayerRole,
    game_env: &mut TEnv,
    conf: &Configuration,
) -> Result<GameResult, Error> {
    // send main to server
    println!("Main: sending name");
    match game_env.send_name(&conf.team_name) {
        Ok(_) => {
            println!("Main: name sent");
        }
        Err(e) => return Err(Error::SendNameError(format!("{}", e))),
    }

    // receive initial state
    let mut response = match GameEnv::<Move, TablutStateMessage>::response(game_env) {
        Ok(res) => res,
        Err(e) => return Err(Error::ResponseError(format!("first state: {}", e))),
    };

    // Player loop
    while response.turn != "WHITEWIN" && response.turn != "BLACKWIN" && response.turn != "DRAW" {
        if (player_role == PlayerRole::White && response.turn == "WHITE")
            || (player_role == PlayerRole::Black && response.turn == "BLACK")
        {
            // white turn
            let state = response.convert_state();

            let action = match player.next_move(state) {
                Some(action) => action, // Game still on
                None => break,          // Game over
            };

            //send moves
            match GameEnv::<Move, TablutStateMessage>::reply(game_env, action) {
                Ok(_) => println!("Main: reply received"),
                Err(e) => {
                    return Err(Error::ReplyError(format!("{}", e)));
                }
            }
        }
        // new state
        // TODO avoid crashing in case of network failure
        response = match GameEnv::<Move, TablutStateMessage>::response(game_env) {
            Ok(res) => res,
            Err(e) => return Err(Error::ResponseError(format!("{}", e))),
        }
    }

    match (response.turn.as_ref(), player_role) {
        ("WHITEWIN", PlayerRole::White) => Ok(GameResult::Wins(Players::Me)),
        ("WHITEWIN", PlayerRole::Black) => Ok(GameResult::Wins(Players::Other)),
        ("BLACKWIN", PlayerRole::Black) => Ok(GameResult::Wins(Players::Me)),
        ("BLACKWIN", PlayerRole::White) => Ok(GameResult::Wins(Players::Other)),
        ("DRAW", _) => Ok(GameResult::Stalemate),
        _ => Err(Error::UnknownResponseError),
    }
}

/// Default configuration file
const DEFAULT_CONF: &str = "Settings.json";

/// Default report export file
const DEFAULT_REPORT: &str = "report.yaml";
const REPORT_FOLDER: &str = "analysis/reports";

// The main needs to know if the player is black or white,
// which kind of player and env to create
// After reading the configuration, the main creates the
// specified structures and connects them,
// then it passes the execution to the app_logic (the controller)
fn main() {
    // read arguments
    // tablut-player <white|black>
    let args: Vec<String> = env::args().collect();
    let program = &args[0];

    let mut options = Options::new();
    options.optopt("c", "conf", "set configuration file", "<CONF_FILE>");
    options.optopt("l", "log", "set log", "<LOG_FILE>");
    options.optopt("s", "server", "server ip", "<IP>");
    options.optopt("p", "port", "server port to bind to", "<PORT>");
    options.optopt("t", "timeout", "set the player's timeout", "<TIMEOUT>");
    options.optopt("", "white_cnn_path", "choose white cnn", "<PATH>");
    options.optopt("", "black_cnn_path", "choose black cnn", "<PATH>");
    options.optflag("h", "help", "print this help");
    options.optflagopt("r", "report", "print mcts_player reports", "[REPORT_FILE]");

    //[> PARSE ARGUMENTS <]

    // check arg length
    if args.len() < 2 {
        print_usage(program, options);
        exit(1);
    }

    // parse player role
    let player_role = match parse_role(&args[1]) {
        Ok(role) => role,
        Err(_) => {
            print_usage(program, options);
            exit(1);
        }
    };

    // parse options
    let matches = match options.parse(&args[2..]) {
        Ok(m) => m,
        Err(_) => {
            print_usage(&program, options);
            exit(1);
        }
    };

    //[> HANDLE OPTIONS <]

    // help flag
    if matches.opt_present("h") {
        print_usage(&program, options);
        return;
    }

    // log option
    if matches.opt_present("l") {
        // TODO handle log
        println!("Log not (yet) implemented!");
    }

    // reports options
    let reports_enabled = matches.opt_present("r");
    let reports_file = if reports_enabled {
        Some(match matches.opt_str("r") {
            Some(str) => str,
            None => String::from(format!(
                "{}/{}_{}_{}",
                REPORT_FOLDER,
                if player_role == PlayerRole::White {
                    "white"
                } else {
                    "black"
                },
                SystemTime::now()
                    .duration_since(SystemTime::UNIX_EPOCH)
                    .unwrap()
                    .as_secs(),
                DEFAULT_REPORT
            )),
        })
    } else {
        None
    };

    // config file
    let conf_file = match matches.opt_str("c") {
        Some(str) => str,
        None => String::from(DEFAULT_CONF),
    };

    //[> CONFIGURATION <]

    // read configuration file
    let mut conf = match read_conf(&conf_file) {
        Ok(c) => c,
        Err(e) => {
            eprintln!("Main: couldn't load configuration: {}", e);
            exit(2);
        }
    };

    conf.time_limit = match matches.opt_str("t") {
        Some(str) => match str.parse::<u64>() {
            Ok(parsed) => parsed,
            Err(e) => {
                print_error(Error::ParsingError(format!(
                    "could not parse time limit argument, using default ({} s): {}",
                    conf.time_limit, e
                )));
                conf.time_limit
            }
        },
        None => conf.time_limit,
    };

    // avoid
    conf.time_limit = conf.time_limit - 2;

    // Print configuration
    print_egg();
    println!("Configuration: {}\n", serde_yaml::to_string(&conf).unwrap());

    let mut port = match player_role {
        PlayerRole::White => conf.white_port.clone(),
        PlayerRole::Black => conf.black_port.clone(),
    };

    // if port option is given override configuration
    port = match matches.opt_str("p") {
        Some(str) => match str.parse::<u16>() {
            Ok(p) => format!("{}", p),
            Err(e) => {
                print_error(Error::ParsingError(format!(
                    "could not parse port argument, using default ({}): {}",
                    port, e
                )));
                port
            }
        },
        None => port,
    };

    // server address
    conf.server_address = match matches.opt_str("s") {
        Some(ip) => ip,
        None => conf.server_address,
    };

    // white_cnn_path
    conf.white_cnn_path = match matches.opt_str("white_cnn_path") {
        Some(path) => path,
        None => conf.white_cnn_path,
    };

    // black_cnn_path
    conf.black_cnn_path = match matches.opt_str("black_cnn_path") {
        Some(path) => path,
        None => conf.black_cnn_path,
    };

    //[>BUILD GAME ENV<]

    let mut game_env = match TEnv::from(format!("{}:{}", conf.server_address, port)) {
        Ok(env) => env,
        Err(e) => {
            eprintln!("Main: couldn't get a game env: {}", e);
            exit(3);
        }
    };

    //[>BUILD PLAYER ROLE<]

    match player_role {
        PlayerRole::White => {
            let mut mcts_player = MonteCarloPlayer::new_with_heuristic(
                reports_enabled,
                BoardWhiteGame::new(), // game
                Board::init(),         // start_state
                CnnPlayer::new(PlayerRole::White, &conf.white_cnn_path)
                    .unwrap()
                    .into_predict_ratio(),
                CnnPlayer::new(PlayerRole::Black, &conf.black_cnn_path)
                    .unwrap()
                    .into_predict_ratio(),
                conf.time_limit, // time_limit
            );

            if conf.white_exploration_ratio.is_some() {
                mcts_player.set_policy(ucb1(conf.white_exploration_ratio.unwrap()));
            }

            println!("Main: mcts_player created");

            let game_result = match app_logic(&mut mcts_player, player_role, &mut game_env, &conf) {
                Ok(result) => {
                    print_result(&result);
                    Some(result)
                }
                Err(e) => {
                    print_error(e);
                    None
                }
            };

            // if reports enabled report reports
            if reports_enabled {
                let reports_file = reports_file.unwrap();
                match report(
                    &reports_file,
                    &mcts_player.report().unwrap(),
                    conf,
                    game_result,
                ) {
                    Ok(_) => println!("Main: report saved: {}", reports_file),
                    Err(e) => print_error(e),
                }
            }
        }
        PlayerRole::Black => {
            let mut mcts_player = MonteCarloPlayer::new_with_heuristic(
                reports_enabled,
                BoardBlackGame::new(), // game
                Board::init(),         // start_state
                CnnPlayer::new(PlayerRole::Black, &conf.black_cnn_path)
                    .unwrap()
                    .into_predict_ratio(), // me_player
                CnnPlayer::new(PlayerRole::White, &conf.white_cnn_path)
                    .unwrap()
                    .into_predict_ratio(), // other_player
                conf.time_limit,       // time_limit
            );

            if conf.black_exploration_ratio.is_some() {
                mcts_player.set_policy(ucb1(conf.black_exploration_ratio.unwrap()));
            }

            // start app logic loop
            let game_result = match app_logic(&mut mcts_player, player_role, &mut game_env, &conf) {
                Ok(result) => {
                    print_result(&result);
                    Some(result)
                }
                Err(e) => {
                    print_error(e);
                    None
                }
            };

            // if reports enabled report reports
            if reports_enabled {
                let reports_file = reports_file.unwrap();
                match report(
                    &reports_file,
                    &mcts_player.report().unwrap(),
                    conf,
                    game_result,
                ) {
                    Ok(_) => println!("Main: report saved: {}", &reports_file),
                    Err(e) => print_error(e),
                }
            }
        }
    }
}
