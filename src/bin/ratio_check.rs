use getopts::Options;
use rand;
use rand::Rng;
use std::env;
use std::error::Error;
use std::fs::OpenOptions;
use std::io::Write;
use std::process::exit;
use std::result::Result;
use tablut_engine::games::tablut;
use tablut_engine::games::tablut::game_board;
use tablut_engine::games::tablut::players;
use tablut_engine::games::tablut::Board;
use tablut_engine::player::mcts_player::simul_agent::*;
use tablut_engine::player::random_player::RandomPlayer;
use tablut_engine::player::{Game, Player, Players};

/* MAIN */

pub fn main() {
    match run() {
        Ok(_) => {}
        Err(e) => {
            println!("Error: {}", e);
        }
    }
}

pub fn run() -> Result<(), Box<dyn Error>> {
    /* MAKE PLAYER, RANDOM TABLUTs */

    let mut rand_gen = rand::thread_rng();

    let white_board_game = game_board::BoardWhiteGame::new();
    let black_board_game = game_board::BoardBlackGame::new();
    let game_w = game_board::BoardWhiteGame::new();
    let game_b = game_board::BoardBlackGame::new();
    let mut rand_white_player =
        RandomPlayer::new(move |s: Board| white_board_game.my_actions(&s).collect());
    let mut rand_black_player =
        RandomPlayer::new(move |s: Board| black_board_game.my_actions(&s).collect());

    let n_simulations = 100;
    let time = 60;

    // compute number of simulations for single cpu
    let n_simulations = if n_simulations == 0 { 1 } else { n_simulations };
    let mut number_of_cpus = num_cpus::get();
    if number_of_cpus > n_simulations as usize {
        number_of_cpus = n_simulations as usize;
    }
    let simuls_for_cpu = n_simulations / number_of_cpus as u32;

    println!("[log] number of cpus used : {}", number_of_cpus);
    println!("[log] simulations per node: {}", simuls_for_cpu);
    let simul_pool_w = SimulationPool::new(
        num_cpus::get() as usize,
        time,
        simuls_for_cpu,
        game_w.clone(),
        players::greedy_player::GreedyWhitePlayer::new(),
        players::greedy_player::GreedyBlackPlayer::new(),
    );
    let simul_pool_b = SimulationPool::new(
        num_cpus::get() as usize,
        time,
        simuls_for_cpu,
        game_b.clone(),
        players::greedy_player::GreedyBlackPlayer::new(),
        players::greedy_player::GreedyWhitePlayer::new(),
    );
    let cnn_white =
        players::cnn_player::CnnPlayer::new(tablut::Player::White, "src/nets/white_trained/graph")
            .unwrap()
            .into_predict_ratio();
    let cnn_black =
        players::cnn_player::CnnPlayer::new(tablut::Player::Black, "src/nets/black_trained/graph")
            .unwrap()
            .into_predict_ratio();

    /* START */

    // generate tablut
    let mut current_board = Board::init();
    let mut current_board_king_out = current_board.clone();
    current_board_king_out.king = 33;
    let num_rand_actions = rand_gen.gen_range(0, 50);
    for _ in 0..num_rand_actions / 2 {
        // white moves
        let white_move = rand_white_player.next_move(current_board.clone());
        if white_move.is_none() {
            break;
        }
        current_board = game_w.apply(&current_board, &white_move.unwrap());

        // black moves
        let black_move = rand_black_player.next_move(current_board.clone());
        if black_move.is_none() {
            break;
        }
        current_board = game_b.apply(&current_board, &black_move.unwrap());
    }

    // generate ratio white and black with simulations
    let mut white_win = 0;
    let mut white_play = 0;
    let mut black_win = 0;
    let mut black_play = 0;

    simul_pool_w.broadcast(&current_board, Players::Me).unwrap();
    simul_pool_b.broadcast(&current_board, Players::Me).unwrap();

    for reply in simul_pool_w.recv().unwrap() {
        white_win += reply.wins;
        white_play += reply.played;
    }

    for reply in simul_pool_b.recv().unwrap() {
        black_win += reply.wins;
        black_play += reply.played;
    }

    // generate ratio white and black with cnn
    let (cnn_ww, cnn_wp) = cnn_white(&current_board);
    let (cnn_bw, cnn_bp) = cnn_black(&current_board);

    println!("Black");
    println!(" simulations wins ratio: {}/{}", white_win, white_play);
    println!(" cnn wins ratio:         {}/{}", cnn_ww, cnn_wp);
    println!();
    println!("White");
    println!(" simulations wins ratio: {}/{}", black_win, black_play);
    println!(" cnn wins ratio:         {}/{}", cnn_bw, cnn_bp);
    println!();

    println!("done.");
    Ok(())
}
