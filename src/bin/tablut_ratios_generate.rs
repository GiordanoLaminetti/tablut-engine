
use getopts::Options;
use rand;
use rand::Rng;
use std::env;
use std::result::Result;
use std::error::Error;
use std::io::{Write};
use std::fs::OpenOptions;
use std::process::exit;
use tablut_engine::player::random_player::RandomPlayer;
use tablut_engine::games::tablut::game_board;
use tablut_engine::games::tablut::players;
use tablut_engine::player::mcts_player::simul_agent::*;
use tablut_engine::games::tablut::{Board};
use tablut_engine::player::{Game, Player, Players};

/* CLI Options */

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} FILE [options]", program);
    print!("{}", opts.usage(&brief));
}

/* UTILS functions */

/* MAIN */

pub fn main() {
    match run() {
        Ok(_) => { },
        Err(e) => {
            println!("Error: {}", e);
        },
    }
}

pub fn run() -> Result<(), Box<dyn Error>> {

    /* PARSING ARGUMENTS */

    // collect arguments
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();
    
    // setup flags
    let mut opts = Options::new();
    opts.optopt("o", "", "set output file name", "NAME.csv");
    opts.optopt("n", "number", "number of examples to generate", "N");
    opts.optopt("s", "simulations", "number of simulations per node", "NUMER_SIMULS");
    opts.optopt("t", "time", "max time to wait for each group of examples in seconds", "SECS");
    opts.optflag("h", "help", "print this help menu");

    let matches = match opts.parse(&args[ 1 .. ]) {
        Ok(m) => m,
        Err(_) => { 
            print_usage(&program, opts);
            exit(1); 
        },
    };

    if matches.opt_present("h") {
        print_usage(&program, opts);
        exit(2);
    }

    // program variables
    let file_name = match matches.opt_str("o") {
        Some(name) => name,
        None => String::from("dataset"),
    };

    let to_gen = match matches.opt_str("n") {
        Some(value) => {
            let to_gen_optional = value.parse::<u32>();
            if let Err(_) = to_gen_optional {
                println!("[error] Invalid number of examples {}", value);
                exit(1)
            }
            to_gen_optional.unwrap()
        },
        None => {
            println!("[info] Number of examples not provided, using {}", 1000);
            1000
        }
    };

    let time = match matches.opt_str("t") {
        Some(value) => {
            let time_optional = value.parse::<u64>();
            if let Err(_) = time_optional {
                println!("Invalid time {}", value);
                exit(1)
            }
            time_optional.unwrap()
        },
        None => {
            println!("[info] limit time to simulation not provided, using {}", 60);
            60
        }
    };

    let n_simulations = match matches.opt_str("s") {
        Some(value) => {
            let n_optional = value.parse::<u32>();
            if let Err(_) = n_optional {
                println!("Invalis simulation number {}", value);
                exit(1);
            } else {
                n_optional.unwrap()
            }
        },
        None => {
            println!("[info] number of simulations not provided .. using {}", 200);
            200
        }
    };

    /* MAKE PLAYER, RANDOM TABLUTs */

    let mut rand_gen = rand::thread_rng();

    let white_board_game = game_board::BoardWhiteGame::new();
    let black_board_game = game_board::BoardBlackGame::new();
    let game_w = game_board::BoardWhiteGame::new();
    let game_b = game_board::BoardBlackGame::new();
    let mut rand_white_player = RandomPlayer::new(move |s: Board| white_board_game.my_actions(&s).collect() );
    let mut rand_black_player = RandomPlayer::new(move |s: Board| black_board_game.my_actions(&s).collect() );

    // compute number of simulations for single cpu
    let n_simulations = if n_simulations == 0 { 1 } else { n_simulations };
    let mut number_of_cpus = num_cpus::get();
    if number_of_cpus > n_simulations as usize {
        number_of_cpus = n_simulations as usize;
    }
    let simuls_for_cpu = n_simulations / number_of_cpus as u32;

    println!("[log] number of cpus used : {}", number_of_cpus);
    println!("[log] simulations per node: {}", simuls_for_cpu);
    let simul_pool_w = SimulationPool::new(
        num_cpus::get() as usize,
        time,
        simuls_for_cpu,
        game_w.clone(),
        players::greedy_player::GreedyWhitePlayer::new_prob(15),
        players::greedy_player::GreedyBlackPlayer::new_prob(70),
    );
    let simul_pool_b = SimulationPool::new(
        num_cpus::get() as usize,
        time,
        simuls_for_cpu,
        game_b.clone(),
        players::greedy_player::GreedyBlackPlayer::new_prob(15),
        players::greedy_player::GreedyWhitePlayer::new_prob(75),
    );

    /* OPEN FILES */

    let mut examples_file_w = match OpenOptions::new()
            .append(true)
            .create(true)
            .open((file_name.clone() + "_white.csv").as_str())  {
        Ok(handle) => handle,
        Err(e) => {
            println!("[warning] cannot create file: {}", e);
            exit(2);
        },
    };

    let mut examples_file_b = match OpenOptions::new()
            .append(true)
            .create(true)
            .open((file_name.clone() + "_black.csv").as_str()) {
        Ok(handle) => handle,
        Err(e) => {
            println!("[warning] cannot create file: {}", e);
            exit(2);
        },
    };

    /* START */
    let mut w_tot_computed = 0;
    let mut b_tot_computed = 0;
    let mut stdout = std::io::stdout();
    while w_tot_computed < to_gen && b_tot_computed < to_gen {

        // generate tablut
        let mut current_board = Board::init();
        let mut current_board_king_out = current_board.clone();
        current_board_king_out.king=33;
        let num_rand_actions = rand_gen.gen_range(0, 40);
        for _ in 0 .. num_rand_actions / 2 {

            // white moves
            let white_move = rand_white_player.next_move(current_board.clone());
            if white_move.is_none() { break }
            current_board = game_w.apply(&current_board, &white_move.unwrap());
            current_board_king_out = game_w.apply(&current_board, &white_move.unwrap());

            // black moves
            let black_move = rand_black_player.next_move(current_board.clone());
            if black_move.is_none() { break }
            current_board = game_b.apply(&current_board, &black_move.unwrap());
            current_board_king_out = game_b.apply(&current_board, &white_move.unwrap());

        }

        // generate ratio white
        let mut white_win = 0;
        let mut white_play = 0;
        let mut black_win = 0;
        let mut black_play = 0;

        simul_pool_w.broadcast(&current_board, Players::Me).unwrap();
        simul_pool_b.broadcast(&current_board, Players::Me).unwrap();
        
        for reply in simul_pool_w.recv().unwrap() {
            white_win += reply.wins;
            white_play += reply.played;
        }

        for reply in simul_pool_b.recv().unwrap() {
            black_win += reply.wins;
            black_play += reply.played;
        }

        // write on files
        examples_file_w.write(Board::to_csv(current_board.clone(), white_win as f64 / white_play as f64).as_bytes()).unwrap();
        examples_file_b.write(Board::to_csv(current_board.clone(), black_win as f64 / black_play as f64).as_bytes()).unwrap();
        
        white_win = 0;
        white_play = 0;
        black_win = 0;
        black_play = 0;

        //simuls king out
        simul_pool_w.broadcast(&current_board_king_out, Players::Me).unwrap();
        simul_pool_b.broadcast(&current_board_king_out, Players::Me).unwrap();
        
        for reply in simul_pool_w.recv().unwrap() {
            white_win += reply.wins;
            white_play += reply.played;
        }

        for reply in simul_pool_b.recv().unwrap() {
            black_win += reply.wins;
            black_play += reply.played;
        }

        // write on files
        examples_file_w.write(Board::to_csv(current_board_king_out.clone(), white_win as f64 / white_play as f64).as_bytes()).unwrap();
        examples_file_b.write(Board::to_csv(current_board_king_out.clone(), black_win as f64 / black_play as f64).as_bytes()).unwrap();
        

        w_tot_computed += 2;
        b_tot_computed += 2;

        stdout.write(format!("\rtot computed : {} / {}", w_tot_computed, to_gen).as_bytes()).unwrap();
        stdout.flush().unwrap();
    }

    println!("done.");
    Ok(())
}
