Trained with all_datasets/dataset_eat_2/white_50_simulations.csv

[train] epoch    1, loss 0.0043 |          119900/         120000 samples | => test set loss: 0.0055
[train] epoch    2, loss 0.0064 |          119900/         120000 samples | => test set loss: 0.0062
[train] epoch    3, loss 0.0031 |          119900/         120000 samples | => test set loss: 0.0055
[train] epoch    4, loss 0.0040 |          119900/         120000 samples | => test set loss: 0.0058
[train] epoch    5, loss 0.0041 |          119900/         120000 samples | => test set loss: 0.0064
[train] epoch    6, loss 0.0044 |          119900/         120000 samples | => test set loss: 0.0061
[train] epoch    7, loss 0.0048 |          119900/         120000 samples | => test set loss: 0.0063
[train] epoch    8, loss 0.0045 |          119900/         120000 samples | => test set loss: 0.0065
[train] epoch    9, loss 0.0043 |          119900/         120000 samples | => test set loss: 0.0072
[train] epoch   10, loss 0.0044 |          119900/         120000 samples | => test set loss: 0.0070
