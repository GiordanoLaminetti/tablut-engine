Trained with all_datasets/mixing/white_dataset/white_dataset_after.csv

using Exponential MSE for errors
[train] epoch    1, loss 0.0039 |          133500/         134028 samples | => test set loss: 0.0070
[train] epoch    2, loss 0.0042 |          133500/         134028 samples | => test set loss: 0.0069
[train] epoch    3, loss 0.0046 |          133500/         134028 samples | => test set loss: 0.0068
[train] epoch    4, loss 0.0034 |          133500/         134028 samples | => test set loss: 0.0073
[train] epoch    5, loss 0.0028 |          133500/         134028 samples | => test set loss: 0.0070
[train] epoch    6, loss 0.0029 |          133500/         134028 samples | => test set loss: 0.0079
[train] epoch    7, loss 0.0030 |          133500/         134028 samples | => test set loss: 0.0073
[train] epoch    8, loss 0.0030 |          133500/         134028 samples | => test set loss: 0.0083
[train] epoch    9, loss 0.0035 |          133500/         134028 samples | => test set loss: 0.0084
[train] epoch   10, loss 0.0027 |          133500/         134028 samples | => test set loss: 0.0085
[train] epoch   11, loss 0.0029 |          133500/         134028 samples | => test set loss: 0.0081
[train] epoch   12, loss 0.0041 |          133500/         134028 samples | => test set loss: 0.0085
[train] epoch   13, loss 0.0026 |          133500/         134028 samples | => test set loss: 0.0085
[train] epoch   14, loss 0.0028 |          133500/         134028 samples | => test set loss: 0.0088
[train] epoch   15, loss 0.0042 |          133500/         134028 samples | => test set loss: 0.0092
[train] epoch   16, loss 0.0025 |          133500/         134028 samples | => test set loss: 0.0087
[train] epoch   17, loss 0.0023 |          133500/         134028 samples | => test set loss: 0.0086
[train] epoch   18, loss 0.0028 |          133500/         134028 samples | => test set loss: 0.0085
[train] epoch   19, loss 0.0028 |          133500/         134028 samples | => test set loss: 0.0098
[train] epoch   20, loss 0.0037 |          133500/         134028 samples | => test set loss: 0.0096
