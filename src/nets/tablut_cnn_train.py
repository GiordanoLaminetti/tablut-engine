"""
Script for training cnn for white or black player
"""

import numpy as np
import tensorflow as tf
import argparse
import itertools
import os, errno, shutil
import sys
import time

from tensorflow.python.saved_model.builder import SavedModelBuilder
from tensorflow.python.saved_model.signature_def_utils import build_signature_def
from tensorflow.python.saved_model.signature_constants import REGRESS_METHOD_NAME
from tensorflow.python.saved_model.tag_constants import TRAINING, SERVING
from tensorflow.python.saved_model.utils import build_tensor_info

## DATASET HANDLING

def load_dataset(file_path):
    """
    Loads a dataset from a csv file and returns a tuple
    with tabluts and ratio
    """
    dataset = np.genfromtxt(file_path, delimiter=',', skip_header=True)
    rows = dataset.shape[0]
    y = dataset[:, -1]                          # ratio labels
    X = dataset[:, 0:-1].reshape(rows, 9, 9, 1) # tabluts
    return (X, y)

def split_train_test(X, y, rate=0.1, seed=0):
    """
    splits dataset in a train and test datasets with a
    given rate
    """
    assert len(X) == len(y)

    # shuffle
    shuffle = np.random.permutation(len(X))
    X = X[shuffle]
    y = y[shuffle]

    # split
    len_test = int(len(X) * rate)
    X_test = X[0:len_test]
    y_test = y[0:len_test]
    X_train = X[len_test:]
    y_train = y[len_test:]

    return X_test, y_test, X_train, y_train

def batch_producer(X, y, batch_size=32):
    """
    produce an iterator over batches
    """
    assert len(X) == len(y)

    num_batches = len(X) // batch_size
    num_examples = batch_size * num_batches
    while True:
        # extract random batch
        indexes = np.random.randint(0, len(X), size=batch_size)
        yield X[indexes], y[indexes]

## BUILD NETWORK

def build_network(name, X):
    """
    Builds a simple CNN network with sigmoid output
    """

    with tf.name_scope(name):

        conv1 = tf.layers.Conv2D(
            filters=16,
            kernel_size=[3, 3],
            strides=(1, 1),
            padding="same",
            use_bias=True,
            activation=tf.nn.relu,
        )(X)

        conv2 = tf.layers.Conv2D(
            filters=32,
            kernel_size=[3, 3],
            strides=(1, 1),
            padding="same",
            use_bias=True,
            activation=tf.nn.relu,
        )(conv1)

        conv3 = tf.layers.Conv2D(
            filters=64,
            kernel_size=[3, 3],
            strides=(1, 1),
            padding="same",
            use_bias=True,
            activation=tf.nn.relu,
        )(conv2)

        flatten = tf.layers.Flatten()(conv3)

        # dense levels
        dense1 = tf.layers.Dense(100, activation=tf.nn.relu)(flatten)
        dense2 = tf.layers.Dense(100, activation=tf.nn.relu)(dense1)

        # logistic output
        ratio = tf.layers.Dense(1, name='ratio')(dense2)

    return ratio

# MAIN FOR TRAINING

# main must load dataset, and train network saving its weights
# this script takes a dataset, if a checkpoint exists load network if not
# creates it
def main():

    # arguments
    parser = argparse.ArgumentParser(description="CNN Tablut training")
    parser.add_argument('player', help='black|white')
    parser.add_argument('dataset', help='csv dataset of examples')
    parser.add_argument('--batch', '-b', type=int, default=100, help='batch size for training')
    parser.add_argument('--epochs', '-e', type=int, default=50, help='number of epochs')
    args = parser.parse_args()

    if args.player not in ['black', 'white']:
        print("[error] player 'black' or 'white'")
        return 1

    # manage folders
    results_folder = 'white_trained' if args.player == 'white' else 'black_trained'
    final_model_folder = results_folder + '/graph'
    checkpoint_file = results_folder + '/checkpoints/model.ckpt'
    log_file = results_folder + '/log.txt'

    os.makedirs(results_folder, exist_ok=True)
    os.makedirs(results_folder + '/checkpoints', exist_ok=True)
    if os.path.exists(final_model_folder):
        print("[warning] found old model, deleted.")
        shutil.rmtree(final_model_folder, ignore_errors=True)
    logger = open(log_file, mode='w')
    logger.write("Trained with {}\n\n".format(args.dataset))

    # load dataset and split it
    try:
        X_examples, y_examples = load_dataset(args.dataset)
        X_test, y_test, X_train, y_train = split_train_test(X_examples, y_examples)
        print('[info] dataset loaded: {} examples for training, {} examples for testing'.format(len(X_train), len(X_test)))
    except OSError:
        print("[error] file {} not found".format(args.dataset))
        return 1

    # TensorFlow setup
    tf.logging.set_verbosity(tf.logging.ERROR)
    config = tf.compat.v1.ConfigProto()
    config.gpu_options.allow_growth = True

    # build graph for network
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    X = tf.placeholder(tf.float32, shape=[None, 9, 9, 1], name="X")    # input batch examples
    y = tf.placeholder(tf.float32, shape=[None, 1], name="y")          # input batch labels
    predicted = build_network("tablut_cnn", X)

    # loss function and optimizer
    with tf.name_scope("training"):
        logger.write("using Exponential MSE for errors\n")
        # loss = tf.losses.mean_squared_error(y, predicted)
        loss = tf.losses.mean_squared_error(y, predicted)
        optimizer = tf.train.AdamOptimizer()
        training_op = optimizer.minimize(loss)

    # compute random batches on dataset
    num_batches = len(X_train) // args.batch
    batches = batch_producer(X_train, y_train, batch_size=args.batch)

    # initializer
    init = tf.global_variables_initializer()

    # saver for checkpoints
    saver = tf.train.Saver()


    # config training
    epochs = args.epochs
    when_save = epochs // 10 if epochs // 10 != 0 else 1
    batch_size = args.batch

    print('[train config] using {} epochs'.format(epochs))
    print('[train config] batch size of {}\n'.format(batch_size))

    # start training
    print('[info] start training CNN')
    with tf.Session(config=config) as sess:
        if os.path.exists(results_folder + '/checkpoints/checkpoint'):
            print('[info] found checkpoint, restoring')
            saver.restore(sess, checkpoint_file)
        else:
            sess.run(init)

        for epoch in range(epochs):

            # train for an epoch
            for batch in range(num_batches):

                # take batch, train
                X_batch, y_batch = next(batches)

                _, loss_val = sess.run([training_op, loss], feed_dict={
                    X: X_batch,
                    y: y_batch.reshape(-1, 1)
                })

            # compute stats on test set
            test_loss_val = sess.run(loss, feed_dict={X: X_test, y: y_test.reshape(-1, 1)})

            to_log = "[train] epoch {:4d}, loss {:06.4f} | {:15d}/{:15d} samples | => test set loss: {:06.4f}".format(epoch + 1, loss_val, batch * batch_size, len(X_train), test_loss_val)

            print(to_log)
            logger.write(to_log + '\n')

            # save ckpt
            if epoch % when_save == 0:
                print('[save] saving model checkpoint in {}'.format(checkpoint_file))
                saver.save(sess, checkpoint_file)

        print('[save] saving complete model and weights in ', final_model_folder)
        tf.saved_model.simple_save(
            sess,
            final_model_folder,
            inputs={"X": X},
            outputs={"out": predicted},
        )
        logger.close()
        print('[complete] done.')

        # some prints for fun
        print('\n[check] random check:')
        board, label = X_test[0], y_test[0]
        label_predicted = sess.run(predicted, feed_dict={X: [board]})[0][0]
        print("  original : {}".format(label))
        print("  predicted: {}".format(label_predicted))

        board, label = X_test[503], y_test[503]
        label_predicted = sess.run(predicted, feed_dict={X: [board]})[0][0]
        print("  original : {}".format(label))
        print("  predicted: {}".format(label_predicted))

        board, label = X_test[1174], y_test[1174]
        label_predicted = sess.run(predicted, feed_dict={X: [board]})[0][0]
        print("  original : {}".format(label))
        print("  predicted: {}".format(label_predicted))
        
    # training ended and model saved, some random tries after reload
    tf.reset_default_graph()
    with tf.Session() as sess:
        print('\n[check] random check after reload:')
        tf.compat.v1.saved_model.load(sess, ['serve'], final_model_folder)
        X = tf.get_default_graph().get_tensor_by_name('X:0')
        output = tf.get_default_graph().get_tensor_by_name('tablut_cnn/ratio/BiasAdd:0')

        board, label = X_test[0], y_test[0]
        label_predicted = sess.run(output, feed_dict={X: [board]})[0][0]
        print("  original : {}".format(label))
        print("  predicted: {}".format(label_predicted))

        board, label = X_test[503], y_test[503]
        label_predicted = sess.run(output, feed_dict={X: [board]})[0][0]
        print("  original : {}".format(label))
        print("  predicted: {}".format(label_predicted))

        board, label = X_test[1174], y_test[1174]
        label_predicted = sess.run(output, feed_dict={X: [board]})[0][0]
        print("  original : {}".format(label))
        print("  predicted: {}".format(label_predicted))


if __name__ == '__main__':
    main()
